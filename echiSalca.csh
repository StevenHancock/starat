#!/bin/csh -f

#############################################################
# This is a shell for controlling salcaRat, the echidna like  #
# ray tracer. It uses ratjoin to recombine into one file    #
# Splitting it up between different processors              #
# 26th October 2006                                         #
#############################################################


#default values#
set ill = 1
set fov = 1
set step = 0
set res = 0.3
@ x = 0
@ y = 0
@ z = 1000
@ rays = 1
@ m = 1
set ill = 1
set fov = 1
set res = 0.3
set lidar = "0 22000 150"
set scans = "-90 90 0 180"
set bands = "wavebands.dat"
set image = " " #no beam image
set ascii = " " #no ascii output
set check =  " " #no ratjoin check
@ test = 0 #default is not to test to see if we're in a tree#
@ zchunk = 1 #do whole scan in one
@ achunk = 1 #do whole scan in one
@ rooter = 0  #default name is object filename
@ runny = 1   #to run ray tracing
@ combine = 1 #run combination program
@ rat = 1     #create grabmes and then possibly trace rays
@ retain = 0  #delete sub files
@ delete = 1  #combine files before deleting sub files
set individual = "nowt"
@ jointeast = 1  #assume failure unless otherwise stated
@ traceteast = 0 #do not delete grab files
@ compglobal = 0 #assume it's all complete
@ filecheck = 1  #check sub files for completness
@ rint = 1       #random number seed
@ withinSegs = 0 #Do not split the beam
@ withoutSegs =1 #Do not split the beam
set truncate = " "
set encod = " "
set psf=" "
set pulselength=" "
set pulseres=" "
set pulsetype = " "
set squint=" "

while ($#argv>0)
  switch("$argv[1]")

  #location of instrument#
  case -position
  case -pos
    set location = "$argv[2] $argv[3] $argv[4]"  #for inaTree#
    set x = `echo $argv[2]|gawk '{printf("%g",$1)}'` #to fit in with ratjoin#
    set y = `echo $argv[3]|gawk '{printf("%g",$1)}'`
    set z = `echo $argv[4]|gawk '{printf("%g",$1)}'`
  shift argv;shift argv;shift argv;shift argv
  breaksw

  #rays per pixel#
  case -rays
    @ rays = $argv[2]
  shift argv;shift argv
  breaksw

  #point spread function#
  case -psf
  case -spread
    set psf = "-psf"
  shift argv
  breaksw

  #ray tree depth#
  case -depth
  case -m
  case -rtd
    @ m = $argv[2]
  shift argv;shift argv
  breaksw

  #field of view#
  case -FOV
  case -fov
  case -view
    set fov = $argv[2]
  shift argv;shift argv
  breaksw

  #beam width#
  case -beam
    set ill = $argv[2]
  shift argv;shift argv
  breaksw

  #beam step size#
  case -step
    set step = $argv[2]
  shift argv;shift argv
  breaksw

  #beam resolution#
  case -res
    set res = $argv[2]
  shift argv;shift argv
  breaksw

  #scan range#
  case -scan
    set scans = "$argv[2] $argv[3] $argv[4] $argv[5]"
  shift argv;shift argv;shift argv;shift argv;shift argv
  breaksw

  #number of rings for each computer#
  case -chunk
  case -scans
    @ zchunk = $argv[2]
    @ achunk = $argv[3]
  shift argv;shift argv;shift argv
  breaksw

  #lidar characteristics#
  case -lidar
  case -ranging
    set lidar = "$argv[2] $argv[3] $argv[4]"
  shift argv;shift argv;shift argv;shift argv
  breaksw

  #sensor head splitting#
  case -split
    @ withinSegs = $argv[2]
    @ withoutSegs = $argv[3]
  shift argv;shift argv;shift argv
  breaksw

  #pulse width#
  case -pulsewidth
  case -pulselength
    set pulselength = "-pulseLength $argv[2]"
  shift argv;shift argv
  breaksw

  #pulse resolution#
  case -pulseres
    set pulseres = "-pres $argv[2]"
  shift argv;shift argv
  breaksw

  #use a lognormal pulse#
  case -lognormal
    set pulsetype = "-lognormal"
  shift argv
  breaksw

  #output name root#
  case -output
    set root = "$argv[2]"
    @ rooter = 1
  shift argv;shift argv
  breaksw

  #image for each beam#
  case -zoom
  case -image
    set image = "-Uzoom $argv[2]"
  shift argv;shift argv
  breaksw

  #object file#
  case -obj
    set objey = "$argv[2]"
  shift argv;shift argv
  breaksw

  #wavebands#
  case -bands
    set bands = "$argv[2]"
  shift argv;shift argv
  breaksw

  #check that ratjoin is working#
  case -check
    set check = "-check"
  shift argv
  breaksw

  #Ascii output#
  case -ascii
    set ascii = "-Uascii"
  shift argv
  breaksw

  #run length encoder#
  case -encode
  case -encod
    set encod = "-encode"
  shift argv
  breaksw

  #test for obstructions#
  case -test
  case -inaTree
    @ test = 1
    set range = $argv[2]
    set tdiv = $argv[3]
  shift argv;shift argv;shift argv
  breaksw

  #to retain sub files#
  case -retain
    @ retain = 1
    @ delete = 0
  shift argv
  breaksw

  #Only create individual data files#
  case -noCombine
  case -justRun
  case -justTrace
    @ combine = 0
    @ jointeast = 0
    @ delete = 0
    @ filecheck = 0
  shift argv
  breaksw

  #only combine existing data files#
  case -noTrace
  case -justCombine
    @ rat = 0
    @ runny=0
  shift argv
  breaksw

  #to clear out sub files without calling ratjoin#
  case -clear
  case -delete
    @ retain = 0
    @ delete = 1
    @ rat = 0
    @ teast = 0
    @ jointeast = 0
    @ filecheck = 0
    @ compglobal = 0
    @ combine = 0
  shift argv
  breaksw

  #to output floats to combined data file#
  case -truncate
  case -float
    set truncate = "-truncate"
  shift argv
  breaksw

  #only produce the grabmes#
  case -grabs
    @ runny  = 0
    @ filecheck = 0
    @ combine = 0
  shift argv
  breaksw

  #to stop subfile checking#
  case -noFileCheck
    @ filecheck=0
  shift argv
  breaksw

  #random number seed#
  case -rint
  case -rand
    @ rint=$argv[2]
  shift argv;shift argv
  breaksw

  #SALCA#
  #width per band#
  case -diffPulses
    set pulselength = "-diffPulses $argv[2] $argv[3]"
  shift argv;shift argv;shift argv
  breaksw

  # laser squint #
  case -squint
    set squint="-squint $argv[2]"
  shift argv;shift argv
  breaksw

  #A help file#
  case -help
    echo " "
    echo "The commands to make echi.rat work are"
    echo "In no particular order, all angles in degrees"
    echo " "
    echo "-scan initial_zenith final_zenith initial_azimuth final_azimuth;"
    echo "-chunk/-scans n_zenith n_azimuth;   the number of processes to split the job into"
    echo "-pos/-position x y z;               must be integers)"
    echo "-rays rays_per_pixel;               an integer"
    echo "-psf;                               gaussian wavefront"
    echo "-depth/-m/-rtd ray_tree_depth       an integer"
    echo "-fov/-FOV/-view field_of_view;      an integer"
    echo "-beam beam_width;                   an angle"
    echo "-step beam_step_size;               an angle"
    echo "-res beam_resolution;               angular size of each beam"
    echo "-lidar/-ranging min max resolution; lidar characteristics"
    echo "-split withinSegs withoutSegs;      split the sensor head"
    echo "-pulselength length;                give the pulse a finite length"
    echo "-pulseres resolution;               default 1/4 of range resolution"
    echo "-lognormal;                         use a lognormal insetad of Gaussian pulse"
    echo "-output name;                       root of the output data file's name"
    echo "-zoom/-image focal_length;          for the beams' images. Leave out for no image"
    echo "-obj name;                          the object"
    echo "-bands waveband_file;               contains spectral information"
    echo "-ascii;                             An ascii output to the screen and a file from starat"
    echo "-test/-inaTree;                     tests to see if we are in a tree first"
    echo "-check;                             ratjoin prints out ascii file to compare to starat ascii"
    echo "-grabs;                             creates the grab files without running them"
    echo "-retain;                            to keep smaller data files after combination"
    echo "-justRun/-noCombine/-justTrace;     do not combine small data files"
    echo "-justCombine/-noTrace;              just combine a list of exisiting data"
    echo "-clear/-delete;                     delete without combining sub files"
    echo "-noFileCheck;                       does not check sub files for completeness"
    echo "-truncate/-float;                   output floats to combined data file rather than doubles"
    echo "-encode/-encod;                     run length encoder"
    echo "-rint/-rand rand_seed;              random number seed"
    echo " "
    echo "SALCA options"
    echo "-diffPulses l1 l2;                  different pulse lengths for 2 bands"
    echo "-squint angle;                      squint angle in degrees"
    echo " "
    echo "Some of these have defaults: These are"
    echo "-scan -90 90 0 180"
    echo "-lidar 0 22000 250"
    echo "-split 0 1"
    echo "-fov 1"
    echo "-beam 1"
    echo "-res 0.3"
    echo "-depth 1"
    echo "-step = -beam"
    echo "-bands wavebands.dat"
    echo " "
    exit;

  default:
    echo "Unknown argument $argv[1]"
    echo "Who knows"
    echo "Type echi.rat -help"
    exit;

  endsw
end

# check the sub division of hemisphere doesn't have remainders
echo $zchunk $step $scans|gawk '{thresh=0.0000001;check=($4-$3)/($1*$2)-int(($4-$3)/($1*$2)+0.5);if((check<-1.0*thresh)||(check>thresh))exit(1);else exit(0)}'
if ( $status && ( $zchunk > 1 ) ) then
  echo "$zchunk does not split the zenith range up into multiples of $step"
  echo "Use a different subdivision with "\""-chunk"\"
  exit
endif
echo $zchunk $step $scans|gawk '{thresh=0.0000001;check=($6-$5)/($1*$2)-int(($6-$5)/($1*$2)+0.5);if((check<-1.0*thresh)||(check>thresh))exit(1);else exit(0)}'
if ( $status && ( $achunk > 1 ) ) then
  echo "$achunk does not split the azimuth range up into multiples of $step"
  echo "Use a different subdivision with "\""-chunk"\"
  exit
endif


if ( ! $rooter ) set root = "$objey" #default is to have it named after object file
if ( $test ) then
  inaTree -pos $location -obj $objey -range $range -div $tdiv
  exit
endif

#step size between segments#
set segsteA = `echo $scans $achunk|gawk '{print ($4-$3)/$5}'`
set segsteZ = `echo $scans $zchunk|gawk '{print ($2-$1)/$5}'`

if ( $rat ) then

  @ checked = 0      #to check there are no holes
  while(! $checked)
    if ( ! -e ./butabe) mkdir ./butabe

    set thisMachine = `uname -n`

    #loop over scan segments to create 
    @ i = 0  #azimuth index#
    #loop over azimuth#
    while ($i < $achunk)
      @ j = 0  #zenith index#
      #then zenith#
      while ($j < $zchunk)

        set segscan=`echo $scans $segsteZ $segsteA $i $j $step $zchunk $achunk|gawk '{if($8<$10-1)fzen=$1+($8+1)*$5-$9;else fzen=$1+($8+1)*$5;if($7<$11-1)faz=$3+$6*($7+1)-$9;else faz=$3+$6*($7+1);printf("%f %f %f %f",$1+$8*$5,fzen,$3+$7*$6,faz);}'`

        set output = "$root.$x.$y.$z.$j.$i"

        set GRABME = "butabe/grab.$root.$x.$y.$z.$j.$i"

        sync
        if ( ! -e $GRABME ) then

          touch $GRABME
          sync
          echo "$GRABME"
          echo "#\!/bin/csh -f"    >  $GRABME
          echo "# "                >> $GRABME
          echo "#Started on `date`">> $GRABME
          echo "#On $thisMachine"  >> $GRABME
          echo "#Full file had the scan range $scans" >> $GRABME
          echo "@ traceteast = 0"  >> $GRABME
          echo "# "                >> $GRABME
          echo "nice +19 salcaRat -RATm $m -Urays $rays -RATsun_position $x $y $z -Uoutput $output -RATtolerance 0.00001 -Ufov $fov -Ubeam $ill -Ures $res -Ustep $step -Ufrom $x $y $z -RATsensor_wavebands $bands -Ulidar $lidar -Uscan $segscan -split $withinSegs $withoutSegs -rint $rint $encod $psf $pulselength $pulseres $pulsetype $image $ascii $squint $objey" >> $GRABME
          echo "# " >> $GRABME
          echo "@ traceteast =" \$status >> $GRABME  #pass the status from shell back here
          echo "if(" \$traceteast ")exit(1)" >> $GRABME
          echo "else exit(0)"           >> $GRABME
          echo "endif"                  >> $GRABME
          echo "# " >> $GRABME
          chmod +x $GRABME
          sync
          if ( $runny ) then
            @ traceteast = 0
            touch $GRABME.running
            echo "On $thisMachine" >> $GRABME.running
            $GRABME
            @ traceteast = $status
            if ( -e disk.full ) then
              echo "The disk is full"
              exit(3);
            endif
            if ( $traceteast ) then
              rm $GRABME
              echo "There was an error somewhere"
              exit(2);
            else
              echo "#"                   >> $GRABME
              echo "#finished on `date`" >> $GRABME
            endif
            rm $GRABME.running
          endif
        endif 
        @ j++
      end
      @ i++
    end
    if ( -e $root.$x.$y.$z.checked.$$ ) @ checked = 1
    if ( ! -e $root.$x.$y.$z.checked.$$ ) then
      touch $root.$x.$y.$z.checked.$$
      echo "Checking that we really have finished"
    endif
  end
  echo "The last ray has been traced"
endif

if ( -e $root.$x.$y.$z.checked.$$) rm $root.$x.$y.$z.checked.$$  #every computer will check through once. Redundant but safe#


#check that all separate files exist and are complete#
#Let user know which are incomplete and delete relevant grabmes#
#Only perform this on one architecture, i86pc for geography, though not always#

#@ archie = `echo $ARCH|gawk '{if($1=="i86pc"||$1=="i686"||$1=="x86_64")print 1;else print 0;}'`
#an alternative method#
#@ archie = `check_endian | gawk '{if($1=="LITTLE") print 1;else print 0;}'`


if ( $filecheck ) then
  if ( ! -e $root.$x.$y.$z.data ) then
    echo "checking that all data files are complete"
    #loop over azimuth#
    @ i = 0
    while ( $i < $achunk )
      #then zenith#
      @ j = 0
      while ( $j < $zchunk )
        @ compteast = 0
        set individual = "$root.$x.$y.$z.$j.$i"
        if ( -e $individual.data ) then
          echiMake -input $individual -header
          @ compteast = $status
          if ( $compteast ) then
            if ( -e butabe/grab.$individual.running ) then
              echo "We think butabe/grab.$individual is still running, is that so?"
              exit(1)
            endif
            rm butabe/grab.$individual
            echo "$individual.data was not complete, you should re-run it"
            @ compglobal=1
          endif
        else if ( ! -e $individual.data ) then
          if ( -e butabe/grab.$individual ) then 
            rm butabe/grab.$individual
          else
            echo "butabe/grab.$individual was not there to be removed"
          endif
          echo "$individual.data had not been done"
        endif
        @ j++
      end
      @ i++
    end
  else
    echo "$root.$x.$y.$z.data is already made, do you really want to check the sub files?"
  endif
endif


#end of sub file checking loop#

#now combine all sub files with ratjoin#
if ( $combine && ( ! $compglobal || ! $filecheck ) ) then
  if ( ! -e $root.$x.$y.$z.data ) then
    touch $root.$x.$y.$z.data  #so that only one computer tries it#

    #Now collect all the separate files into one large binary file#
    #checking the headers to make sure they are all the same      #
    @ archCheck=`echo $ARCH|gawk '{if($1=="i386")print 1;else print 0}'`
    if ( $archCheck ) then
      echo "ratjoin does not work on $ARCH, use another machine"
      exit(1)
    endif

    echo "nice +19 ratjoin -scan $scans -chunk $zchunk $achunk -step $step -input $root -lidar $lidar -pos $x $y $z $check $truncate"

    nice +19 ratjoin  -scan $scans -chunk $zchunk $achunk -step $step -input $root -lidar $lidar -pos $x $y $z $check $truncate
    @ jointeast = $status
  else 
    echo "There is already a $root.$x.$y.$z.data, is another computer creating it?"
  endif
endif

if ( $delete ) then
  if ( ( ! $retain ) && ( ! $jointeast ) && ( ! $compglobal ) ) then
    #get rid of individual files#
    echo "Deleteing sub files..."
    #loop over azimuth#
    @ i = 0
    while ( $i < $achunk )
      #then zenith#
      @ j = 0
      while ( $j < $zchunk )

        set individual = "$root.$x.$y.$z.$j.$i.data"
        if ( -e $individual ) then
          rm $individual
          echo "removed $individual"
        else if ( ! -e $individual ) then 
          echo "$individual was not there to be deleted"
        endif
        @ j++
      end
      @ i++
    end
  endif
endif

echo "The program has got to it's end"
if ( ( $jointeast && ( $runny && $combine)) || $traceteast ) then
  echo "There was an error wasn't there"
  echo " "
  exit(1)
endif
echo " "
exit(0)
#######



