#
#
#       Makefile for librat example program
#
#
#

# users will probably want to change this
# make sure ${BIN}/${ARCH} exists
HOME=/Users/stevenhancock
BIN = ${HOME}/bin
LEWIS_LIB = ${HOME}/src/RATlib/src/lib
TOOLS=${HOME}/src/headers

LIBS = -limage_${ARCH} -lmatrix_${ARCH} -lalloc_${ARCH} -lvect_${ARCH} -lerr_${ARCH} -lhipl_${ARCH} -lrand_${ARCH} -lrat_${ARCH}  -lm -lc 
CFLAGS += -L${LEWIS_LIB} -D${ARCH} -D_NO_NAG

INCLS = -I${LEWIS_LIB}/rat -I${TOOLS} -I${LEWIS_LIB}

#CFLAGS += -g
CFLAGS += -O3 
CFLAGS += -Wall
CC=gcc
THIS = starat

#	build executables

THIS:		
		make clean ${THIS} install


# conical tree test
HET01_DIS_ERE:		${THIS}
		${THIS} < test.ip -random 1 -v 1  -m 100 -sensor_wavebands wavebands.dat HET01_DIS_ERE.obj

sick:
			@echo "stick your fingers down your throat"

${THIS}:		makefile ${THIS}.o #../headers/${TOOLS}
#		$(CC) $(CFLAGS) -I${LIBRARY}  -L. -L${LIBRARY}/lib ${THIS}.o -o $@ ${LIBS}
		$(CC) $(CFLAGS) -L. ${THIS}.o -o $@ ${LIBS} ${INCLS}

# object building

.c.o:			makefile $<
			$(CC) ${CFLAGS} ${INCLS} -o $@ -c $<

.f.o:			makefile $<
#			f77 ${FFLAGS} -I${LIBRARY}/lib -c $< 
			f77 ${FFLAGS} -I$(LEWIS_LIB) -c $<

install:
	touch  ${BIN}/${ARCH}/${THIS}
	mv ${BIN}/${ARCH}/${THIS} ${BIN}/${ARCH}/${THIS}.old
	cp ${THIS} ${BIN}/${ARCH}/${THIS}

clean:		
		rm -f *% *.o

