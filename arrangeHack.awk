BEGIN{
  nNodes=0;
  nComp=0;
  nTodo=0;

  for(i=0;i<achunk;i++){
    for(j=0;j<zchunk;j++){
      todo[i,j]=0;
    }
  }
}



($0){

  if($1=="##"){   # count computers
    node[nComp]=$3;
    nNodes+=node[nComp];
    nComp++;
  }else if($1!="#"){
    aTodo[nTodo]=$1;
    zTodo[nTodo]=$2;
    nTodo++;
  }
}


END{
  thisFrac=node[n]/nNodes;
  nThis=int(nTodo*thisFrac);

  # modfiy those that aren't to be done
  if(n!=0)start=n*nThis;
  else    start=0;

  if(n<(nComp-1))stop=(n+1)*nThis;
  else           stop=nTodo;


  print start,stop;
  for(i=start;i<stop;i++){
    todo[aTodo[i],zTodo[i]]=1;
  }

  for(i=0;i<achunk;i++){
    for(j=0;j<zchunk;j++){
      if(todo[i,j]==0)printf("grab.%s.%d.%d %d %d\n",root,j,i,j,i);
      #else printf("Hey up\n");
    }
  }

}

