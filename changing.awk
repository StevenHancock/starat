#######################################################
#An awk script for finding two lines that are the same#
#for any ascii input                                  #
# 20th October 2006                                   #
#######################################################


BEGIN{
  shadow=0;
}

{
x[shadow]=$5;
shadow++;
}
END{

for(i=1;i<shadow;i++){
  if(x[i]==x[i-1]&&x[i+1]!=x[i-2])printf("Your problem is on line %d\n",i);
}



}
