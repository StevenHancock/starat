BEGIN{
  scale=1.0;
}
{
  step=$2
  while((step*scale-int(step*scale))>0.0)scale*=10.0;
  print scale,step

}
