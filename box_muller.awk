#!/usr/bin/gawk -f
# box_muller...http://www.taygeta.com/random/gaussian.html

BEGIN{
	srand();
	mean=0.9274;
	#mean=0;
	sigma=0.2660;
	
}
#POLAR FORM OF BOX M
{	
	date=$1;
	do {
		x1 = 2.0 * rand() - 1.0;
    	x2 = 2.0 * rand() - 1.0;
    	w = x1 * x1 + x2 * x2;
  print w,x1,x2
	} while (w >= 1.0)
	
	w2 = sqrt( (-2.0 * log( w ) ) / w ) * x1;
    gauss = w2 * sigma + mean;
	
	print date, gauss;
	#print gauss
}

