




/*######################################################################*/
/* These are the structures needed by starat, echiMake and ratjoin.     */
/*######################################################################*/


/*#######################################*/
/*# Copyright 2006-2016, Steven Hancock #*/
/*# The program is distributed under    #*/
/*# the terms of the GNU General Public #*/
/*# License.    svenhancock@gmail.com   #*/
/*#######################################*/


/*########################################################################*/
/*# This file is starat.                                                 #*/
/*#                                                                      #*/
/*# starat is free software: you can redistribute it and/or modify       #*/
/*# it under the terms of the GNU General Public License as published by #*/
/*# the Free Software Foundation, either version 3 of the License, or    #*/
/*#  (at your option) any later version.                                 #*/
/*#                                                                      #*/
/*# starat is distributed in the hope that it will be useful,            #*/
/*# but WITHOUT ANY WARRANTY; without even the implied warranty of       #*/
/*#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the       #*/
/*#   GNU General Public License for more details.                       #*/
/*#                                                                      #*/
/*#    You should have received a copy of the GNU General Public License #*/
/*#    along with starat.  If not, see <http://www.gnu.org/licenses/>.   #*/
/*########################################################################*/


typedef struct{
#ifndef VOXELS
  double theta;   /*zenith for this ray*/
  double thata;   /*azimuth for this beam*/
#endif
  double *refl;   /*array of reflectances at range*/
  float sky;      /*percentage of direct rays that reach the sky*/
  float *material;/*percentage of total(includes blank space) for each material for each bin*/
  int coords[3];  /*compressed data coordinates*/
  int compLeng[2];/*length of compressed arrays*/
#ifdef STARAT
  double *shade;        /*segment brightness for image*/
  double pixPos[2];     /*pixel polar coords*/
  unsigned char *image; /*the actual image bit*/
#endif
#ifdef VOXELS
  int *voxN;     /*number of voxels this bin lies within*/
  int **voxMap;   /*2-D map of which bin is in which voxel*/
  float **fraction;/*fraction of overlap between a bin and voxel*/
#endif
} RatResults;

#ifdef RATMAKER
  typedef struct{   /*holds features of a beam*/
    int nFeat;      /*number of features*/
    int **pos;      /*bin of start, maximum and end*/
    double *max;    /*maximum amplitude*/
    double *lead;   /*amplitude of leading edge*/
    double *trail;   /*amplitude of trailing edge*/
    double totalE;    /*total beam energy*/
    double *energy;    /*energy within a feature*/
    double **certainty; /*in the feature being a type*/
    double peak;       /*maximum intensity of waveform*/
  }RatFeat;
#endif

typedef struct{
  double from[3]; /*starting point*/
  double div;     /*field of view, degrees (in starat at least)*/
  double Ldiv;    /*beam width in degrees(radians out of starat)*/
  double divSt;   /*step size*/
  double iZen;    /*start and finish scan angles*/
  double fZen;    /*scan range*/
  double iAz;     /*scan range*/
  double fAz;     /*scan range*/
  double max_R;    /*maximum lidar range*/
  double min_R;    /*minimum lidar range*/
  double bin_L;    /*lidar bin length   */
  float squint;    /*squint angle for SALCA*/
  int bins;        /*number of bins*/
  int nBands;       /*number of wavebands*/
  double *wavelength;/*wavelengths*/
  int n;            /*ray tree depth, for transfer between*/
  int NZscans;     /*Number of beams needed for zenith scan*/
  int NAscans;    /*Number of beams for azimuth scan*/
  int nMat;       /*number of materials in the object*/
  char encoded;   /*0 if uncompressed, 1 if run length encoded for input*/
  char encod;     /*same as above for output file*/
  RatResults *ratRes;  /*pointer to the results*/
  double *theta;      /*angles to save RAM*/
  double *thata;     /*flamin' RAM!*/
  int withinSegs;    /*number of detector sensors within beam angle*/
  int withoutSegs;   /*number of detector sensors without beam angle*/
  int headerLength;  /*length of integer header (10 or 12 ints)*/
  int header2Length; /*length of double header (10+nBands doubles)*/
#ifdef STARAT
  /*###here lie all the variables needed only within starat###*/
  /*   Each image pixel is of size 1, zoom controls number    */
  char byteord;      /*0 little endian, 1 big endian*/
  unsigned int Npixs;/*Number of segments in one beam*/
  int Ipixs;        /*Number of beam image pixels*/
  int *Nsegs;      /*Number of annuli, Nrings1,N2,...*/
  char Loutput[200];/*lidar data output*/
  FILE *output;    /*ouput file pointer*/
  FILE *Ioutput;  /*image file pointer*/
  FILE *Soutput;  /*data structure file*/
  float zoom;     /*focal length, sort of*/
  int imageSi;    /*Length of side of image*/
  int *rayN;      /*number of rays per pix*/
  float meanrayN; /*mean number of rays per pixel*/
  char psf;       /*allows none square beams*/
  double res;     /*angular pixel size*/
  double *dthe;   /*zenith of annuli*/
  double *dtha;   /*azimuth step in annuli*/
  double *idthe;  /*zenith of annuli within image*/
  double *idtha;  /*azimuth stpe within image*/
  int *InSegs;    /*number of segments in each image annulus*/
  int INpixs;     /*number of pixels in round image*/
  int Ascii;      /*turns ascii output on/off*/
  int test;       /*to see if we are in a tree*/
  double maxshade;/*maximum pixel brightness for image*/
  double Ediv;    /*effective divergence (to 1/e2) for psf*/
  char intLidar;  /*allow old integer lidar parameter format*/
  char sampled;   /*sample waveform return switch*/
  double (*funk)(double,double,double);   /*pointer to pulse shape function*/
  int rint;       /*random number seed*/
#endif
  float *Plength; /*pulse width, in units*/
  int pBands;     /*number of different pulses, for SALCA*/
  float Pres;     /*resolution of the pulse shape, default 1 unit*/
  int *EpLength;  /*effective pulse width (to zero)*/
  float **pulse;  /*pulse shape*/
#ifdef VOXELS
/*  int angles; */     /*number of angles tored right now*/
/*  float *theta; */  /*to save RAM*/
/*  float *thata; */  /*to save RAM*/
  float *albedo;    /*albedo for each element at each wavelength*/
  float **ro;      /*reflectance of each material at each wavelength*/
  float *Etheta;   /*zenith thetas for ellipsoidal fun*/
  int *matCode;   /*converts between data and canopy materials*/
#endif
#ifdef RATMAKER
  RatFeat **ratFeat;  /*waveform feature information*/
  int nFeaTypes;      /*number of types to classify features into*/
  char **feaTypes;    /*list of feature types*/
  int coarsenRemain;  /*remainder of range coarsening*/
  double **maxAmp;    /*hold maximum amplitudes for deconvolution iterations*/
#endif
} RatControl;

#ifdef RATMAKER
  typedef struct{ /*output for data header*/
    int bins;
    int NZscans;
    int NAscans;
    int n;
    int nBands;
    int nMat;
    int withinSegs;
    int withoutSegs;
    int EpLength;
    double from[3];
    double div;
    double divSt;
    double iZen;
    double fZen;
    double iAz;
    double fAz;
    double *wavelength;
    double Plength;
    double Pres;
  } RatHead;
#endif

#ifdef VOXELS
  /*the voxel representation*/
  typedef struct{
    float pos[3];/*position of voxel's centre*/
    float *Ae;   /*element area for each material*/
    float *LAD;  /*average angle of incidence from a direction for each material*/
    float psi;   /*Lambert's woody correction*/
    int contN;    /*number of range bins within this voxel*/
    long coord;    /*position of rayMap within the file*/
    int *rayMap;    /*map of which rays are within the voxel*/
    char adjust;     /*indicates multiple zeniths passing through*/
    float *fraction; /*fractional overlap of voxel and range bins*/
/*    int type;  */  /*1 soft, 2 hard, 0 no information*/
/*    float clump;*/ /*I don't yet know how to treat this one*/
  } Voxel;
#endif

typedef struct{
  int *matConvert; /*material index map*/
  char **matType;  /*labels for LUT*/
  char *subdir;    /*subdir filename for indicators*/
  char plants[200];/*name of the material information file*/
  int nsamples;    /*to use Lewis' volumetric functions*/
  int *nTypes;     /*number of types for each material*/
  #ifndef FORAGE     /*then we are in echiMake*/
    int **types;   /*type indices for each material*/
  #else              /*we are in forage*/
    char ***types; /*primitive type for object*/
  #endif             /*BEWARE THIS DIFFERENCE*/
  float **meanSize;/*mean size of each shape and material*/
} RatVox;

#ifdef RATMAKER
  /*for the hemispherical results, echiMake*/
  typedef struct{
    char memorymap;    /*if 1 arrays emptied and cleared, else arrays kept*/

    FILE *monochrome;  /*monochrome image file*/
    FILE *colour;       /*colour image file*/
    FILE *materialI;     /*material image file*/
    float *image;         /*pixel brightness buffer (as we don't know max)*/
    unsigned char *imageV;/*pixel brightness*/
    int *Ncont;           /*number of contributions to pixels*/
    unsigned char *imageC;/*colour lidar image pixel brightness*/
    int *NcontC;          /*number of contributions to colour pixels*/
    unsigned char *imageM;/*material image pixel brightness*/
    float *tempMI;        /*temporary material image array*/
    int *NcontM;          /*number of contributions to material pixels*/
    int imageSi;          /*length of side in pixels*/
    double maxZen;        /*radians from centre to edge*/
    int mzoom;            /*focal length, for monochrome image (on/off)*/
    int czoom;            /*focal length, for colour image (on/off)*/
    int pzoom;            /*focal length, for panoramic image (on/off)*/
    int matzoom;          /*focal length for material image (on/off)*/
    int *matRGB[3];       /*material index to paint red, green and blue*/
    int nRGB[3];          /*number of red, green and blue materials*/
    int gain;            /*to control image brightness*/
    int view;           /*1, use Ldiv; 0, use div*/
    double point;      /*direction of central beam*/
    char input[100];  /*binary data input name*/
    FILE **data;     /*array of binary data input pointers*/
    char ascii;     /*turns ascii output on in echiMake*/
    char asciiSave; /*only prints out filled bins, needs ascii*/
    char asciiIn;   /*read in ascii rather that binary data*/
    char jusAng;    /*makes ascii output only scanned angles*/
    char totalFlux; /*calculate total flux for each band*/
    char stats;     /*print out beam betweenscan stats*/
    char rawStats;  /*print out the raw reflectance values used to calculate stats*/
    char intent;    /*print out the message of intent (nee joy) or not*/
    double Arange[4];/*ascii scan range, zenith and azimuth*/
    char printHeader;/*print header information to the screen*/
    double *Hprof;  /*array of maximum heights*/
    int Hdim[2];   /*x and y dimensions of height profile*/
    float Hpos[2]; /*x and y coordinates of profile centre*/
    int **Hmap;    /*bin to pixel map for height profile*/
    int *HbinN;    /*number of bins to each profile pixel*/
    int Hres;      /*height profile bin size*/
    char teast;    /*To not read the angles (only read header*/
    char height;   /*to calculate tree height or not, used in forage*/
    char scatFract;/*turn on scattering fraction calculator*/
    int scanN;     /*number of scans to look through*/
    int coarsen;    /*a range resolution coarsening factor*/
    char checkD;     /*to check the data for errors*/
    char badMat;     /*indicates files with incorrectly normalised material information*/
    char byteord;    /*0 little endian, 1 big endian*/
    char idealFile;  /*if an ideal file is included for comparison, not analysis*/
    char *output;    /*output file root*/
    char *groundout; /*filename for ground estimate output*/
    int withinSegs;  /*to spit sensor head in any way*/
    int withoutSegs; /*to split sensor head as you want*/
    double noise;    /*percentage amplitude of noise to add*/
    int seed;        /*random number seed*/
    char photon;     /*convert reflectance to photon count*/
    #ifdef VOXELS    /*##it seems strange to bury voxels here, but it may become useful##*/
      char linearVoxels; /*use fudgey linear LAD model when inverting voxels*/
      char species;     /*separates coniferous and broadleaves*/
      float errThresh; /*Threshold for least squares*/
      int NthetaLUT;  /*number of angles in phase LUT*/
      int NmLUT;       /*number of eccentricities in phase LUT*/
      float **phaseLUT;/*phase look up table*/
      float *thetaLUT; /*angle label for above*/
      float *mLUT;     /*LAD label for above*/
      Voxel *voxel;    /*voxel representation*/
      float voxRes;     /*voxel dimensions (square), turns it on/off*/
      float voxScene[3]; /*dimensions of voxel scene, x, y, z*/
      float voxCentre[3];/*centre coordinate of voxel scene*/
      int voxNumb[3];   /*number of voxels in x, y, z*/
      int totalVoxels; /*exactly what it says*/
      int matN;        /*number of intreresting elements*/
      float *ends;    /*the x, y, z bounds (xmin, xmax, ymin...*/
      double cos2mu; /*mu is the crossover angle for thetaHat*/
      double mu;      /*mu is the crossover angle for thetaHat*/
      float *omegaRatio;/*LUT for LAD to eccentricity*/
      int angles;      /*number of angles stored right now*/
/*      double *theta; */ /*angles to save RAM*/
  /*    double *thata; */ /*flamin' RAM!*/
/*      FILE *rayMapFile;*/  /*RAM, again*/
      FILE *voxMapFile;  /*Beatify Gates for with him there is no RAM!*/
      char Vinput[100]; /*binary voxel data file name*/
      char Vfile;       /*indicates there is a source file*/
/*      int *rayMap; */  /*the map that once was within voxels. RAM!!!*/
      int vzoom;     /*pixels per voxel side*/
      int zenN;      /*for showing the LAD*/
      float *acAe;   /*clumping corrected surface area*/
      char vrml;     /*turns vrml output on and off*/
      char fixclump; /*turns sub bin clumping correction on/off*/
      char createMap;/*to let it know if we need to create a new map*/
      char checkMap; /*print out an ascii voxel map or not*/
      char Vascii;   /*print out an ascii voxel file*/
      char Vlog;     /*print voxels for each iteration*/
      char Vflat;    /*when drawing draw all layers in a single image*/
      RatVox *voxRat;/*holds information for reading object file*/
      char anyAdjustable;/*indicates whether there is any point checking gaps*/

      /*echidna bits*/
      char phaseHet;  /*test heterognty of phase function*/
      char trueGap;   /*calculate true gap fraction*/
      float tGapres;   /*zenith resolution of true gap fraction*/
      char juppGap;     /*caluclate gap fraction by Jupp et al, 2009*/
      float standRadius; /*radius of plot to use for juppGap*/
      float jGapres;    /*zenith resolution of Jupp gap fraction*/

    #endif

    char gnucoords;   /*prints out coords for gnuplot*/
    int nBands;       /*number of bands to use in inversion*/
    int **band;       /*indices for use with each scan*/
    float Athresh;    /*A-scope height threshold*/
    float noiseRange; /*range to which there should be no signal*/
    int noiseBins;    /*length of backnoise arrays*/
    double **backnoise;/*set of background noise values*/
    double *meanoise; /*mean noise levels for each band of one beam*/
    double *stdevnoise;/*standard deviation for each band of one beam*/
    float **solarI;   /*solar irradiance to calculate background noise*/
    char noisestats;  /*calculate noise statistics*/
    char denoise;     /*use empty range to subtract noise*/
    char sigstart;    /*print out start of signal*/
    float ndevs;      /*number of standard deviaitons to use as start threshold*/
    char unTracked;   /*find signal start without noise tracking*/
    double *noithresh;/*noise threshold for denoising*/
    char GaussFit;    /*provide initial estimates of Gaussians to fit with fitG*/
    char *plants;     /*name of plant material file*/
    char ***refFiles; /*relectance files for each material*/
    char LUTlegend;   /*outputs a material LUT*/

    /*for ilydar*/
    char pixelShape;  /*0, round, 1 square*/

    /*stuff for waveform maths*/
    char dividewave;  /*divide two waves and output to a file (held in band)*/ 
    char spectralGround; /*find ground through topography with two bands*/
    char spectralGround2; /*find ground through topography with two bands*/
    char ratiout;     /*output spectral ratio and derivatives*/
    char windowstat;  /*caluclate windowed stats*/
    char weighty;     /*apply the weighted smoothing during spectral ratio*/

    /*stuff for deconvolution*/
    float Plength;       /*for deconvolving returns*/
    float conlength;      /*for convolveing*/
    float postlength;       /*for post deconvolution smoothing*/
    float noilength;         /*Gaussian length for smoothing after de-noising*/
    float pestlength;         /*Gaussian length for smoothing before parameter estimation*/
    float *pulse;               /*pulse shape for deconvolution*/
    float Pres;                    /*deconvolution pulse resolution*/
    char sdecon;                     /*deconvolve using pulse in file*/
    char deconLaplace;                /*deconvolve by convolution with the Laplace of the pulse*/
    int diters;                        /*iterations for deconvolution*/
    char deconOrders;                   /*deconvolve different orders of scattering*/
    char convergeCheck;                  /*check for convergence against pulse length*/
    char stopGold;                       /*use max Gaussian energy to stop iterating*/
    char jansson;                        /*use Jansson's method instead of Gold's, includes maxAmp of stopGold*/
    unsigned long int EpLength;          /*deconvolution pulse length (with resolution)*/
    double (*funk)(double,double,double); /*pulse shape*/
    char outdecon;    /*save deconvolved waveforms to a new data file*/
    char features;   /*analyse waveform for features*/
    float clip;      /*fraction of max frequency to clip off*/
    float Geiger;    /*threshold for Geiger mode APDs*/
    int addGeiger;    /*combine Geiger counts into a waveform*/
    char firstReturn;  /*just measure the first return*/
    char discreteInt;   /*retain the intensity from discrete lidar*/
    float ratioBand[2]; /*bands to caluclate the ratio between*/
    float medRatio;     /*median filter width for use on spectral ratio*/
    char waveMaths;     /*carry out maths on wavwebands*/
    double *crop;       /*ranges to crop between*/
    RatHead *newpoint;  /*new header information*/
    int nGauss;         /*number of Gaussians to fit to a wave by powell's*/
    float **GaussParams;/*parameters of fitted Gaussians after decomposition*/

    float *trans;       /*atmospheric transittance*/

  } RatImage;
#endif

#ifdef RATCOMBINE
  /*for use in ratjoin, which combines data files into one*/
  typedef struct{
    char input[100];  /*data filename root*/
    int zchunk;       /*number of zenith chunks*/
    int achunk;       /*number of azimuth chunks*/
    double iZen;      /*start and finish scan angles, global*/
    double fZen;      /*global scan range*/
    double iAz;       /*global scan range*/
    double fAz;       /*global scan range*/
    int NZscans;      /*total number of zenith scans*/
    int NAscans;      /*total number of azimuth scans*/
    double step;      /*beam step*/
    double pos[3];    /*x, y, z position*/
    int check;        /*if 1 prints out lidar information*/
    int ind;          /*To show two consecutive missing data files*/
    int sky;          /*a temporary hash to keep old files compatible*/
    char byteord;     /*0 for little endian, 1 for big endian*/
    char mapping;     /*1 if we are only reading headers, 0 for all data*/
    int *coords;      /*global coordinates for output*/
    char *ratCheck;   /*to check the map*/
    int headerLength; /*integer header length*/
    int header2Length;/*double header length*/
    int withinSegs;   /*split sensor head*/
    int withoutSegs;  /*split sensor head*/
    int EpLength;     /*pulse array length*/
    double Plength;   /*pulse length*/
    double Pres;      /*pulse resolution*/
    float *pulse;     /*pulse shape*/
  } RatCombine;
#endif

/*a buffer for holding swapped binary data*/
/*typedef struct{
//  int *header1;         
//  double *header2;
//  RatResults *ratRes;  
//}RatByte;  */
