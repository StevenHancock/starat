/* this first line is required in the main() file */
#define RAT_MAIN
/* you need to include this file */
#include "rat.h"
#define STARAT
#include "starat.h"
#include "tools.h"
#include "tools.c"

#define THRESHOLD 0.00001

/*###################################################################################*/
/*# This is a forward ray tracer, using P Lewis' RAT library                        #*/
/*# it can scan a hemisphere of lidar beams. Outputting the results as a binary file#*/
/*# it can also ouput as an ascii if it is desired. Hips images can be produced for #*/
/*# beam, but beware of the amount of space this will take up                       #*/
/*###################################################################################*/


int main(int argc,char **argv){
  void drawPic(RatControl *,RatResults *,int,char **);
  void userSignals(),doStuff(RATobj *),controlPrepare(RatControl *,RATtree *,RATobj *);
  void binaryResults(RatResults *,RatControl *,int,float,float,double *,float *,int);
  char byteOrder();
  void tidyUpArrays(RatControl *,RATobj *,RATtree *,RatResults *);
  RATobj *ratObj=NULL;
  RATtree *ratTree=NULL;
  RatControl *ratPoint=NULL,*pointPrepare();
  RatResults *ratRes=NULL,*resultPrepare(RatControl *,RATtree *);
  void pointat(RATobj *,RatControl *,RATtree *,RatResults *);
  int *intSwap(int *,int);
  float *floSwap(float *,int);
  double *doSwap(double *,int);
  long int *lintSwap(long int*,int);
  void openOutput(RatControl *);


  /* this function for setting up signal interrupts if you want such things */
  /* you then need to define a fn userSignals() and also RATuserInterrupt()
  ** examples of which are below
  ** if you dont want these, just define a dummy RATuserInterrupt() function
  */
  userSignals();

/*
#  if(sizeof(long int)!=4){
#    printf("long integers are the wrong length on this system\n");
#    exit(1);
#  }*/

  /* RAT initialisation and memory allocation */
  ratObj=RATinit(argc,argv);
  ratPoint=pointPrepare();

  /* check default frat arguments
  ** user parsing is done within this function by calls to RATuserParse()
  ** with help options etc. specified in RATuserPrintOptions()
  ** examples of which are below
  */

  RATparse(ratObj,argc,argv,ratPoint);
  ratTree=RATgetRatTree(ratObj);

  /* read the data from the object file (if specified) */
  if(!(RATisWavefrontFile(ratObj))){
    printf("No object specified.\n");
    exit(1);
  }else {
    printf("\nReading object file.\n");
    RATreadObject(ratObj);
  }
  printf("Read it.\n\n");


  controlPrepare(ratPoint,ratTree,ratObj);  /*set up control structure*/
  ratPoint->byteord=byteOrder();            /*determine endianess*/

  /*open all the output files, to make sure we have the space*/
  if(!ratPoint->test)openOutput(ratPoint);

  /*prepare results array*/
  ratRes=resultPrepare(ratPoint,ratTree);

  /*The ray tracing command # trace # trace # trace # trace # trace # trace*/
  pointat(ratObj,ratPoint,ratTree,ratRes);

  /*write the header information, only after all beams have been traced*/
  if(!ratPoint->test)binaryResults(ratRes,ratPoint,-1,0,0,NULL,NULL,0);  /*write the header to binary file*/

  /*create the image*/
  if((ratPoint->zoom>0.0)&&(!ratPoint->test))drawPic(ratPoint,ratRes,argc,argv);

  /*empty arrays*/
  tidyUpArrays(ratPoint,ratObj,ratTree,ratRes);

  return(0);
}/*main*/


/*###############################################################################*/

void pointat(RATobj *ratObj,RatControl *ratPoint,RATtree *ratTree,RatResults *ratRes)
{
  double direction[3],vect[3];
  double L=1,ddthe=0,sTep=0;
  double pie=M_PI,stepSi=0;
  double rsTep=0,rddthe=0;
  double x=0,y=0,z=0;
  double *refl=NULL;    /*reflectance array*/
  float *material=NULL; /*material array*/
  float theta=0,thata=0; /*beam angles to trace*/
  float thetaOut=0,thataOut=0; /*beam angles to output, different if squinted*/
  float cosSq=0,tanSq=0; /*trig of squint angle*/
  float arg=0;           /*pre-calculate argument for squint*/
  int n=0,m=0,r=0;
  int i=0,l=0;
  int lPlace=0;
  int refLen=0,matLen=0;   /*array lengths*/
  int sky=0;               /*sky counter*/
  unsigned int count=0;
  unsigned int saveStuff(RATobj *,RatControl *,RATtree *,unsigned int,int,double *,float *,int *);
  void binaryResults(RatResults *,RatControl *,int,float,float,double *,float *,int);
  void inaTree(RatControl *,RatResults *,int);
  void asciiWrite(RatResults *,RatControl *,int);

  register int NAscans=ratPoint->NAscans,j=0; /*this is the most accessed variable, so register it*/

  /*some default values, just in case*/
  if(!ratPoint->Ldiv){
    printf("No beam divergence defined!\n");
    exit(1);
  }
  if(ratPoint->div==0)ratPoint->div=ratPoint->Ldiv*180/M_PI;
  if(ratPoint->res==0)ratPoint->res = 0.01;
  if(!L)L = 1; /*vector length, possibly brightness*/

  /*##############message of intent, formerly joy############################################*/
  printf("Tracing rays from %g %g %g\nOver the angles %g to %g zenith \nand %g to %g azimuth\n\
  With %d wavebands of wavelength",ratPoint->from[0],ratPoint->from[1],ratPoint->from[2],\
  ratPoint->iZen*180/pie,ratPoint->fZen*180/pie,ratPoint->iAz*180/pie,ratPoint->fAz*180/pie,\
  ratPoint->nBands);
  for(i=0;i<ratPoint->nBands;i++) fprintf(stdout," %g",ratPoint->wavelength[i]);
  printf("\nWith a beam divergence of %g and FOV of %g\nThere will be %d beams in groups of\
  %d and %d\nThe lidar is measuring from %g to %g in steps of %g\n",ratPoint->Ldiv*180/M_PI,\
  ratPoint->div,ratPoint->NZscans*NAscans,ratPoint->NZscans,NAscans,\
  ratPoint->min_R,ratPoint->max_R,ratPoint->bin_L);
  printf("The sensor has been split into %d within and %d without\n",ratPoint->withinSegs,\
  ratPoint->withoutSegs);
  printf("With a pulse lengths of");
  for(i=0;i<ratPoint->pBands;i++)fprintf(stdout," %g",ratPoint->Plength[i]);
  fprintf(stdout,"\n");
  if(ratPoint->psf)printf("The beam has  Gaussian wavefront\n");
  /*############intent has been stated#######################################################*/

  srand(ratPoint->rint);
  ratTree=RATgetRatTree(ratObj);

  /*allocate temporary arrays*/
  refLen=(ratPoint->withinSegs+ratPoint->withoutSegs)*ratPoint->bins*ratPoint->nBands*(ratTree->n+1);
  refl=dalloc(refLen,"refl",0);
  matLen=(ratPoint->withinSegs+ratPoint->withoutSegs)*ratPoint->bins*ratPoint->nMat*2;
  material=falloc(matLen,"material",0);

  /*pre-alculate squint angles*/
  cosSq=cos(ratPoint->squint);
  tanSq=tan(ratPoint->squint);

  /*step over the beams, zenith then azimuth*/
  for(l=0;l<ratPoint->NZscans;l++){
    fprintf(stdout,"\nScanning ring with a zenith of %f ",(ratPoint->iZen+l*ratPoint->divSt)*180/M_PI);
    //fflush(stdout);
    for(j=0;j<NAscans;j++){
      /*fprintf(stdout,"\nScanning ring with an angle of %f %f ",(ratPoint->iZen+l*ratPoint->divSt)*180/M_PI,\
                                                  (ratPoint->iAz+j*ratPoint->divSt)*180/M_PI);
      fflush(stdout);*/
      lPlace=l*NAscans+j;

      if(ratPoint->squint<THRESHOLD){   /*some tolerance for rounding*/
        theta=thetaOut=ratPoint->iZen+(double)l*ratPoint->divSt;
        thata=thataOut=ratPoint->iAz+(double)j*ratPoint->divSt;
      }else{                            /*there is a SALCA squinbt angle*/
        thetaOut=ratPoint->iZen+(double)l*ratPoint->divSt;  /*the value we want*/
        thataOut=ratPoint->iAz+(double)j*ratPoint->divSt;   /*the value we want*/
        arg=cos(thetaOut)*cosSq;
        if(thetaOut>=0.0)theta=M_PI/2.0-atan2(arg,sqrt(1.0-arg*arg));  /*angles with squint*/
        else             theta=atan2(arg,sqrt(1.0-arg*arg))-M_PI/2.0;  /*some adjusting needed for -ve zen*/
        if(thetaOut>=0.0)thata=thataOut+(M_PI-atan2(tanSq,sin(thetaOut))); /*angles with squint*/
        else             thata=thataOut+atan2(tanSq,sin(thetaOut));
fprintf(stdout,"Ang %f %f %f %f\n",thetaOut*180.0/M_PI,thataOut*180.0/M_PI,theta*180.0/M_PI,thata*180.0/M_PI);
thetaOut=theta;
thataOut=thata;
      }
      /*set vector for FOV test*/
      vect[0]=sin(theta)*cos(thata);
      vect[1]=sin(theta)*sin(thata);
      vect[2]=cos(theta);
      RATsetNsuns(ratObj,1);
      RATsetSun(ratObj,vect);

      count=0;  /*reset counters*/
      sky=0;

      /*within a beam step over zeniths*/
      for(m=1;m<=ratPoint->Nsegs[0];m++){
        ddthe=ratPoint->dthe[m-1];
        stepSi=ratPoint->dtha[m-1];

        /* step around the circumference */
        for(n=0;n<ratPoint->Nsegs[m];n++){
          sTep = ((double)n+0.5)*stepSi;
          /*loop through rays in a pixel*/
          for(r=0;r<ratPoint->rayN[m-1];r++){

            /*Apply randomness to ddthe and sTep, once only, then use these in place*/
            rddthe=ddthe+ratPoint->res*((double)rand()/(double)RAND_MAX-0.5);
            rsTep=sTep+stepSi*((double)rand()/(double)RAND_MAX-0.5);

            x=sin(rddthe)*cos(rsTep);
            y=sin(rddthe)*sin(rsTep);
            z=cos(rddthe);

            direction[0]=cos(thata)*(x*cos(theta)+z*sin(theta))-y*sin(thata);
            direction[1]=sin(thata)*(x*cos(theta)+z*sin(theta))+y*cos(thata);
            direction[2]=z*cos(theta)-x*sin(theta);


            /* trace */
            /* trace */
            RATtraceRay(ratObj,ratPoint->from,direction,NULL);
            ratTree=RATgetRatTree(ratObj);
            /* trace */
            /* trace */
            /*then save the results somewhere*/
            count=saveStuff(ratObj,ratPoint,ratTree,count,l*NAscans+j,refl,material,&sky);
          }
        }
      }
      /*now write the results to a binary file*/
      /*This is to reduce the amount of RAM needed*/
      if(!ratPoint->test){
//fprintf(stdout,"Ang %f %f\n",thetaOut*180.0/M_PI,thataOut*180.0/M_PI);
        binaryResults(ratRes,ratPoint,lPlace,thetaOut,thataOut,refl,material,sky);
        if(ratPoint->Ascii) asciiWrite(ratRes,ratPoint,lPlace);
      } else if(ratPoint->test) inaTree(ratPoint,ratRes,lPlace);  /*obstruction test*/
      /*reset arrays*/
      for(m=0;m<refLen;m++)refl[m]=0.0;
      for(m=0;m<matLen;m++)material[m]=0.0;
    }/*az loop*/
  }/*zen loop*/

  TIDY(refl);
  TIDY(material);

  fprintf(stdout,"\nPing\n\n");
  return;
}  /*pointat*/


/*#####################################################################################*/
/*transfer individual ray information to a results array*/

unsigned int saveStuff(RATobj *ratObj,RatControl *ratPoint,RATtree *ratTree,unsigned int count,int place,double *refl,float *material,int *sky)
{
  int k=0,j=0,i=0,n=0;     /*loop variables*/
  int d=0;             /*shows direct/diffuse for material usage*/
  int pBand=0;         /*pulse band*/
  double range=0,angle=0;
  int reflLength=0,materialLength=0; /*length of data arrays for ease of offset*/
  void convolvePulse(RATtree *,RatControl *,double *,int,int,int,int,int,double,int);
  void shadeInPic(RatControl *,RATtree *,RatResults *,int,int,int);

  reflLength=ratPoint->bins*ratPoint->nBands*(ratPoint->n+1);
  materialLength=ratPoint->bins*ratPoint->nMat*2;

  /*step through ray tree depth*/
  for(j=0;j<=ratTree->thisRTD;j++){
    if(180.0-ratTree->angleToSun[j]<=ratPoint->div/2.0){   /*test to see if we're within the beam width*/
      range+=ratTree->rayLengths[j];
      /*to avoid rounding issues in the convolution k is not used for convolved light*/
      k=(int)((range+ratTree->lengthToSun[j]-ratPoint->min_R)/ratPoint->bin_L+0.5); /*the bin*/
      if(k<0) k=0;                                                                  /*       */
      if(k>ratPoint->bins-1) k=ratPoint->bins-1;                                    /*the bin*/
      if(j==0)d=0;
      else    d=1;
      /*then see which segment to allocate the return to*/
      for(n=0;n<ratPoint->withinSegs+ratPoint->withoutSegs;n++){
        if(!ratPoint->withinSegs)angle=ratPoint->div*M_PI/360.0;   /*if we are not splitting the sensor*/
        else if(n<ratPoint->withinSegs)angle=(double)(n+1)*(ratPoint->Ldiv/2.0)/(double)ratPoint->withinSegs;
        else angle=ratPoint->Ldiv/2.0+(double)(n+1-ratPoint->withinSegs)*(ratPoint->div*M_PI/360.0-ratPoint->Ldiv/2.0)\
                   /(double)ratPoint->withoutSegs;
        if(180.0-ratTree->angleToSun[j]<=angle*180.0/M_PI){  /*the beam is inside this segment*/
          /*and through wavebands*/
          for(i=0;i<ratPoint->nBands;i++){
            if(ratTree->directRadiance[j*ratPoint->nBands+i]>0.0){
              if(ratPoint->pBands>1)pBand=i;
              else                  pBand=0;
              if(ratPoint->Plength[pBand]==0.0){
                refl[n*reflLength+(ratTree->n+1)*(k*ratPoint->nBands+i)]+=ratTree->directRadiance[j*ratPoint->nBands+i];
                refl[n*reflLength+(k*ratPoint->nBands+i)*(ratTree->n+1)+j+1]+=ratTree->directRadiance[j*ratPoint->nBands+i];
              }else if(ratPoint->Plength[pBand]>0.0){
                convolvePulse(ratTree,ratPoint,refl,place,n,i,d,j,range+ratTree->lengthToSun[j]-ratPoint->min_R,pBand);
              }else{printf("bad pulse width %f\n",ratPoint->Plength[pBand]);exit(1);}
              /*material information is never convolved*/
              material[n*materialLength+k*2*ratPoint->nMat+2*ratTree->ratmat[j]+d]++;
            }
          }/*band loop, "i"*/
          break;  /*once we have the right segment exit*/
        } /*which segment check*/
      }   /*segment step, "n"*/
      if(ratPoint->zoom>0.0){
        fprintf(stderr,"Pictures are turned off for now\n");
        exit(1);
        /*shadeInPic(ratPoint,ratTree,ratRes,place,j,k);*/
      }
    }/*outer angle checking loop*/
  }  /*scattering depth step, "j"*/
  if(ratTree->thisRTD<0)(*sky)++;
  count++;
  return(count);
}/*saveStuff*/


/*##############################################################*/
/*fill in the relevant pixels*/

void shadeInPic(RatControl *ratPoint,RATtree *ratTree,RatResults *ratRes,int place,int j,int k)
{                                                                    /*beam, scatter depth, bin*/
  int band=0,m=0;
  int iPlace=0;
  int zePlace=0,azPlace=0;
  double x=0,y=0,z=0;
  double xT=0,yT=0,zT=0;
  double zen=0,az=0;
  double cthe=0,ctha=0,sthe=0,stha=0;
  double acosArg=0;                    /*to prevent roundinig errors*/

  cthe=cos(ratRes[place].theta);
  ctha=cos(ratRes[place].thata);
  sthe=sin(ratRes[place].theta);
  stha=sin(ratRes[place].thata);

  x=ratTree->intersectionPoints[j][0]-ratPoint->from[0];  /*set the from point at the origin*/
  y=ratTree->intersectionPoints[j][1]-ratPoint->from[1];
  z=ratTree->intersectionPoints[j][2]-ratPoint->from[2];  /*then rotate the beam to be facing vertically downwards*/
  xT=cthe*(x*ctha+y*stha)-z*sthe;
  yT=y*ctha-x*stha;
  zT=sthe*(x*ctha+y*stha)+z*cthe;
  
  zen=M_PI*(1.0-ratTree->angleToSun[j]/180.0);

  if(zT<THRESHOLD||(zen<0||zen>ratPoint->div*M_PI/360.0)){
    printf("bad zenith angle in image drawing %f of %f vector %f %f %f\n",zen,ratPoint->div*M_PI/360.0,xT,yT,zT);
    exit(1);
  }
  if(yT*yT+xT*xT>THRESHOLD*THRESHOLD){
    acosArg=xT/sqrt(yT*yT+xT*xT);
    if(acosArg>=-1.0||acosArg<=1.0);
    else if(acosArg<-1.0)acosArg=-1.0;
    else if(acosArg>1.0)acosArg=1.0;
    if(yT>0)az=acos(xT/sqrt(yT*yT+xT*xT));
    else    az=2.0*M_PI-acos(xT/sqrt(yT*yT+xT*xT));
  }else     az=0; 
  if(az<0||az>2.0*M_PI){
    printf("bad azimuth angle in image drawing %f\n",az);
    exit(1);
  }
  zePlace=ratPoint->InSegs[0]-1-(int)(zen/ratPoint->res+0.5);
  if(zePlace==-1)zePlace=0;                /*to deal with tiny rounding issues*/
  else if(zePlace==ratPoint->InSegs[0])zePlace=ratPoint->InSegs[0]-1;
  else if(zePlace<0||zePlace>=ratPoint->InSegs[0]){
    printf("bad annulus %d zen %f res %f Nsegs %d\n",zePlace,zen,ratPoint->res,ratPoint->InSegs[0]);
    exit(1);
  }
  if(ratPoint->InSegs[zePlace+1]>1){
    if(ratPoint->idtha[zePlace]<THRESHOLD){
      printf("bugger it %d %f %d %f %f\n",zePlace,ratPoint->dtha[zePlace],ratPoint->Nsegs[zePlace+1],zen,ratPoint->res);
      exit(1);
    }
    azPlace=(int)(az/ratPoint->idtha[zePlace]+0.5);
  }else azPlace=0;
  if(azPlace==-1)azPlace=0;
  else if(azPlace==ratPoint->InSegs[zePlace+1])azPlace=ratPoint->InSegs[zePlace+1]-1;
  else if(azPlace<0||azPlace>=ratPoint->InSegs[zePlace+1]){
    printf("azimuth position error %d of %d\n",azPlace,ratPoint->InSegs[zePlace+1]);
    exit(1);
  }
  iPlace=0;                                          /*set the shade locaton*/
  for(m=1;m<=zePlace;m++){
    iPlace+=ratPoint->InSegs[m];
  }
  iPlace+=azPlace;
  if(iPlace<0||iPlace>=ratPoint->INpixs){
    printf("Oh dear, image place error %d %d\n",iPlace,ratPoint->Npixs);
    exit(1);
  }
  for(band=0;band<ratPoint->nBands;band++){
    ratRes[place].shade[iPlace+band*ratPoint->INpixs]+=ratTree->directRadiance[j*ratPoint->nBands+band];
    if(ratRes[place].shade[iPlace+band*ratPoint->INpixs]>ratPoint->maxshade)\
      ratPoint->maxshade=ratRes[place].shade[iPlace+band*ratPoint->INpixs];
  }

  return;
}/*shadeInPic*/


/*################################################################################*/
/*give convolve impulse with a finite pulse*/

void convolvePulse(RATtree *ratTree,RatControl *ratPoint,double *refl,int place,int n,int i,int d,int j,double range,int pBand)
{
  int p=0;
  int plert=0;
  int reflLength=0,materialLength=0;

  reflLength=ratPoint->bins*ratPoint->nBands*(ratPoint->n+1);
  materialLength=ratPoint->bins*ratPoint->nMat*2;

  for(p=0;p<ratPoint->EpLength[pBand];p++){
    plert=(int)(((double)((float)p-((float)ratPoint->EpLength[pBand]/2.0))*(double)ratPoint->Pres+range)/ratPoint->bin_L+0.5);
    /*plert=(int)(((double)p*(double)ratPoint->Pres+range)/ratPoint->bin_L+0.5);*/
    if(plert<0)plert=0;
    else if(plert>=ratPoint->bins)plert=ratPoint->bins;
    if(plert>=0&&plert<ratPoint->bins){               /*make sure we are in the beam*/
      refl[n*reflLength+(ratPoint->n+1)*(plert*ratPoint->nBands+i)]+=\
        ratTree->directRadiance[j*ratPoint->nBands+i]*(double)ratPoint->pulse[pBand][p];
      refl[n*reflLength+(plert*ratPoint->nBands+i)*(ratPoint->n+1)+j+1]+=\
        ratTree->directRadiance[j*ratPoint->nBands+i]*ratPoint->pulse[pBand][p];
    } /*else{*/         /*if not we have lost a bit of energy*/
     /* printf("ENERGY LEAK! ");
      if(plert<0)printf("under ");
      else       printf("over ");
    }*/
  }

  return;
} /*convolvePulse*/


/*#############################################################################*/
/*put the circular shade into a square image*/

void drawPic(RatControl *ratPoint,RatResults *ratRes,int argc,char **argv)
{
  int x=0,y=0;
  int i=0,k=0,j=0,n=0,num=0,brEak=0,l=0;
  double r=0,az=0,pie=M_PI;
  double maxR=0,minR=0,maxAz=0,minAz=0;
  double acosArg=0;
  struct header hd;
  char namen[200];
  void cleanPics(RatControl *);
  void fp_fwrite_header();

  printf("\nDrawing the image\n\n");

  /*loop through image pixels*/
  for(i=0;i<ratPoint->Ipixs;i++){
    x=i%ratPoint->imageSi-ratPoint->imageSi/2;
    if(!x)y=(int)((float)i/(float)ratPoint->imageSi-(float)ratPoint->imageSi/2.0);
    else  y=(int)((float)i/(float)ratPoint->imageSi-(float)ratPoint->imageSi/2.0)+1;
    if(x==0&&y==0)az=0;
    else{
      acosArg=(double)x/sqrt((double)(y*y+x*x));
      if(y>0)  az=acos((double)x/sqrt((double)(y*y+x*x)));
      else     az=2.0*pie-acos((double)x/sqrt((double)(y*y+x*x)));
    }
    r=sqrt((double)(x*x+y*y));
    if(az<0||az>2.0*M_PI){printf("az error %f\n",az);exit(1);}

    k=0;
    brEak=0;
    /*step through annuli*/
    for(j=1;j<=ratPoint->InSegs[0];j++){
      /*here we have a choice about which projection, ensure image size is suitable*/  
    /*# maxR=ratPoint->zoom*(tan(ratPoint->dthe[k]+ratPoint->res/2));
      # minR=ratPoint->zoom*(tan(ratPoint->dthe[k]-ratPoint->res/2)); */
      maxR=ratPoint->zoom*(ratPoint->idthe[j-1]+ratPoint->res/2.0)*180.0/pie;
      minR=ratPoint->zoom*(ratPoint->idthe[j-1]-ratPoint->res/2.0)*180.0/pie;
      /*check we are in right annulus*/
      if(r>maxR){
        brEak=1;
        break;
      }
      if(r>minR&&r<=maxR){  /*step through segments*/
        /*If yes step around annulus*/
        for(num=k;num<k+ratPoint->InSegs[j];num++){
          if(num>ratPoint->INpixs||num<0){
            printf("num error\n");
            exit(1);
          }
          maxAz=(double)(num-k+1)*ratPoint->idtha[j-1];
          minAz=(double)(num-k)*ratPoint->idtha[j-1];
          if(az>=minAz&&az<=maxAz){
            for(n=0;n<ratPoint->nBands;n++){
              /*allocate pixel for all beams*/
              for(l=0;l<ratPoint->NZscans*ratPoint->NAscans;l++){
         /*      if(num%2)ratRes[l].shade[num+n*ratPoint->Npixs]=0; */ /*to make pretty pictures*/
                ratRes[l].image[i+n*ratPoint->Ipixs]=(unsigned char)(ratRes[l].shade[num+n*ratPoint->INpixs]*\
                  255.0/ratPoint->maxshade);                              /*this is to normalise*/
              }
            }
            brEak=1;
            break;
          }
        }
      }
      k+=ratPoint->InSegs[j];
      if(brEak)break;
    }
  }
  for(l=0;l<ratPoint->NZscans*ratPoint->NAscans;l++){
    if(ratRes[l].shade){
      free(ratRes[l].shade);
      ratRes[l].shade=NULL;
    }
  }

  /*then print out the images*/
  for(i=0;i<ratPoint->NZscans*ratPoint->NAscans;i++){
    sprintf(namen,"%s.%d.%d.%d.%d.hip",ratPoint->Loutput,(int)(ratPoint->Ldiv*180/M_PI+0.5),\
      (int)(ratPoint->div),(int)(ratRes[i].theta*180/M_PI+0.5),(int)(ratRes[i].thata*180/M_PI+0.5));
    if((ratPoint->Ioutput=fopen(namen,"wb"))==NULL){
      printf("Error opening image output\n");
      exit(1);
    }
    init_header(&hd,ratPoint->Ioutput," ",ratPoint->nBands,"today",ratPoint->imageSi,\
      ratPoint->imageSi,8*sizeof(unsigned char),0,PFBYTE," ");
    update_header(&hd,argc,argv);
    fp_fwrite_header(ratPoint->Ioutput,&hd);
    if(fwrite(ratRes[i].image,sizeof(unsigned char),ratPoint->Ipixs*ratPoint->nBands,ratPoint->Ioutput)!=ratPoint->Ipixs*ratPoint->nBands){
      printf("image not written\n");
      exit(1);
    }
    if(ratRes[i].image){
      free(ratRes[i].image);
      ratRes[i].image=NULL;
    }
    printf("The images have %d pixels each.\n",ratPoint->Ipixs);
    if(ratPoint->Ioutput){
      fclose(ratPoint->Ioutput);
      ratPoint->Ioutput=NULL;
    }
  }
  cleanPics(ratPoint);

  return;
}/*drawPic*/


/*###################################################################################*/
/*prepare the control structure*/

RatControl *pointPrepare()
{
  RatControl *out=NULL;
  double gaussian(double,double,double);
  if(out){
    free(out);
    out=NULL;
  }
  if(!(out=(RatControl *)calloc(1,sizeof(RatControl)))){
    fprintf(stderr,"error in control allocation.\n");
    exit(1);
  }
  /*defaults*/
  if(!(out->rayN=(int *)calloc(1,sizeof(int)))){
    fprintf(stderr,"error in ray control allocation.\n");
    exit(1);
  }
  out->rayN[0]=1;   /*one ray per pixel*/
  out->psf=0;      /*square wavefront*/
  out->pBands=1;   /*infinitely sharp pulse*/
  out->Plength=falloc(1,"pulse",0);  /*infinitely sharp pulse*/
  out->Plength[0]=0.0;  /*infinitely sharp pulse*/
  out->EpLength=ialloc(1,"pulse",0);  /*infinitely sharp pulse*/
  out->EpLength[0]=0;
  out->Pres=-1.0; /*flag to make pulse res 1/4 of bin_L*/
  out->zoom=0;    /*no image*/
  out->Ascii=0;    /*no ascii output*/
  out->test=0;     /*assume we're not in a tree*/
  out->encoded=0;  /*no run length encoding*/
  out->withinSegs=0;
  out->withoutSegs=1;
  out->intLidar=0;   /*use doubles for lidar parameters*/
  out->funk=gaussian;
  out->rint=0;
  out->squint=0.0;   /*no squint angle*/
  /*  strcpy(out->Loutput,"results"); */

  return(out);
}/*pointPrepare*/


/*###################################################################################*/
/*prepare the results structure and set values in the point structure*/

RatResults *resultPrepare(RatControl *ratPoint,RATtree *ratTree)
{
  RatResults *out=NULL;
  void preparePic(RatControl *,RatResults *);

  TIDY(out);
  if(!(out=(RatResults *)calloc(ratPoint->NZscans*ratPoint->NAscans,sizeof(RatResults)))){
    fprintf(stderr,"error in results allocation 1.\n");
    exit(1);
  }
  if(!ratPoint->test){
    if(ratPoint->zoom>0.0)preparePic(ratPoint,out);
  }

  return(out);
}  /*resultPrepare*/


/*#############################################################################################*/
/*prepare the image*/

void preparePic(RatControl *ratPoint,RatResults *ratRes)
{
  int k=0;
  int annuli=0;      /*annuli in image*/
  double halfWidth=0;/*half FOV in radians*/

  halfWidth=ratPoint->div*M_PI/360.0;

  /*the image size to be careful of, depending on image projection*/
/*    ratPoint->imageSi=(int)(2*ratPoint->zoom*tan(ratPoint->Ldiv/2)+1);*/ /*round up*/
  ratPoint->imageSi=(int)((double)ratPoint->zoom*ratPoint->div+1.0);
  ratPoint->Ipixs=ratPoint->imageSi*ratPoint->imageSi;


  /*this is repeating setPixelNumbers, but neater to have it here*/
  annuli=(int)((halfWidth-ratPoint->res/2.0)/ratPoint->res)+1;
  if(!(ratPoint->InSegs=(int *)calloc(annuli+1,sizeof(int)))){
    printf("error in image allocation 1.\nMayhap it is too big?\n");
    exit(1);
  }
  if(!(ratPoint->idthe=(double *)calloc(annuli,sizeof(double)))){
    printf("error in image allocation 13.\nMayhap it is too big?\n");
    exit(1);
  }
  if(!(ratPoint->idtha=(double *)calloc(annuli,sizeof(double)))){
    printf("error in image allocation 12.\nMayhap it is too big?\n");
    exit(1);
  }
  ratPoint->InSegs[0]=annuli;
  for(k=1;k<=annuli;k++){
    if(k<ratPoint->InSegs[0]){
      ratPoint->idthe[k-1]=halfWidth-((double)k+0.5)*ratPoint->res;
      ratPoint->InSegs[k]=(int)(2.0*M_PI*sin(ratPoint->idthe[k-1])/(double)ratPoint->res);
      ratPoint->idtha[k-1]=2.0*M_PI/(double)ratPoint->InSegs[k];
    }else if(k==ratPoint->InSegs[0]){
      ratPoint->InSegs[k]=1;
      ratPoint->idthe[k-1]=0;
      ratPoint->idtha[k-1]=2.0*M_PI;
    }
    ratPoint->INpixs+=ratPoint->InSegs[k];
  }


  for(k=0;k<ratPoint->NZscans*ratPoint->NAscans;k++){
    /*The refl allocation is done just before each beam, to save memory*/
    /*allocate image holding arrays if they are needed*/
    /*When this option is used we probably won't be using too many beams, so memory is not a problem*/
    if(!(ratRes[k].shade=(double *)calloc(ratPoint->INpixs*ratPoint->nBands,sizeof(double)))){
      printf("error in image allocation 3.\nMayhap it is too big?\n");
      exit(1);
    }
    if(!(ratRes[k].image=(unsigned char *)calloc(ratPoint->Ipixs*ratPoint->nBands,sizeof(unsigned char)))){
      printf("error in image allocation 4.\nMayhap it is too big?\n");
      exit(1);
    }
  }
  if(ratPoint->NAscans*ratPoint->NZscans>1){
    printf("For now the images only work for single beams, not %d\n",ratPoint->NAscans*ratPoint->NZscans);
    exit(1);
  }
  return;
}  /*preparePic*/


/*#############################################################################################*/
/*prepare controls*/

void controlPrepare(RatControl *ratPoint,RATtree *ratTree,RATobj *ratObj)
{
  int i=0,annuli=0;
  void setSpreadFunction(RatControl *,int);
  void setPulseLength(RatControl *,int);
  void preparePic(RatControl *,RatResults *);
  int setPixelNumbers(RatControl *);

  ratPoint->wavelength=(double *)v_allocate(MAX_WAVEBANDS,sizeof(double));
  ratPoint->nBands=RATgetNWavebands(ratObj,ratPoint->wavelength);
  ratPoint->nMat=RATgetNmaterials(ratObj);

  if(ratPoint->divSt==0)ratPoint->divSt=ratPoint->Ldiv;
  ratPoint->bins=(int)((ratPoint->max_R-ratPoint->min_R)/ratPoint->bin_L)+2;
  ratPoint->NZscans=(int)((ratPoint->fZen-ratPoint->iZen)/ratPoint->divSt+0.5)+1;
  ratPoint->NAscans=(int)((ratPoint->fAz-ratPoint->iAz)/ratPoint->divSt+0.5)+1;
  if(!ratPoint->NZscans)ratPoint->NZscans=1; /*default is a single scan*/
  if(!ratPoint->NAscans)ratPoint->NAscans=1;  /*default is single scan*/
  ratPoint->n=ratTree->n;
  /*if the beam divergence is greater than the FOV then we can't split the outer ring*/
  if(ratPoint->withinSegs>0&&ratPoint->Ldiv*M_PI/180.0>=ratPoint->div){
    printf("You can't have that splitting (%d %d) with beam divergences of %f and fov of %f\n",\
     ratPoint->withinSegs,ratPoint->withoutSegs,ratPoint->Ldiv*180.0/M_PI,ratPoint->div);
    exit(1);
  }

  /*count up number of pixels needed*/
  annuli=setPixelNumbers(ratPoint);

  ratPoint->headerLength=13+ratPoint->pBands;
  ratPoint->header2Length=15+ratPoint->nBands+ratPoint->pBands;

  setSpreadFunction(ratPoint,annuli);
  for(i=0;i<ratPoint->pBands;i++)if(ratPoint->Plength[i]>0.0)setPulseLength(ratPoint,i);


  if((ratPoint->pBands>1)&&(ratPoint->pBands!=ratPoint->nBands)){
    fprintf(stderr,"Band mismatch\n");
    exit(1);
  }else if(ratPoint->pBands==0){
    fprintf(stderr,"What?\n");
    exit(1);
  }

  return;
}/*controlPrepare*/


/*#######################################################################################*/
/*count up number of pixels needed*/

int setPixelNumbers(RatControl *ratPoint)
{
  int i=0,annuli=0,Nannuli=0,ann=0;
  int E=0;
  double ddthe=0,pie=M_PI;
  double sigma=0;

  ratPoint->Npixs=0;
  if(!ratPoint->psf){
    annuli=(int)((ratPoint->Ldiv/2.0-ratPoint->res/2.0)/ratPoint->res)+1;
  }else{
    sigma=ratPoint->Ldiv/4.0;
    /*find the number of annuli actually needed (includes blur over edge)*/
    for(i=0;i<100000;i++){
      if((float)(i*i)*ratPoint->res*ratPoint->res/(2.0*sigma*sigma)<105049){
        E=(int)(2.0*(double)ratPoint->rayN[0]*gaussian((double)i*(double)ratPoint->res,sigma,0)+0.5);
        /*E=(int)(2.0*(double)ratPoint->rayN[0]*exp(-1.0*(float)(i*i)*ratPoint->res*ratPoint->res/(2.0*sigma*sigma))+0.5);*/
      }else{
        E=0;
      }
      if(E<=0){
        annuli=i;
        break;
      }
    }
    ann=(int)((ratPoint->Ldiv/2.0-ratPoint->res/2.0)/ratPoint->res)+1; /*the segments up to 1/e2 */
  }
  if(!(ratPoint->Nsegs=(int *)calloc(annuli+1,sizeof(int)))){
    printf("error  annuli in allocation.\n");
    exit(1);
  }
  ratPoint->Nsegs[0]=annuli;   /*Nsegs[0] is the length of the array Nsegs*/
  if(!(ratPoint->dthe=(double *)calloc(ratPoint->Nsegs[0],sizeof(double)))){
    printf("error in image allocation 1.\n");
    exit(1);
  }
  if(!(ratPoint->dtha=(double *)calloc(ratPoint->Nsegs[0],sizeof(double)))){
    printf("error in image allocation 2.\n");
    exit(1);
  }
  for(i=1;i<=ratPoint->Nsegs[0];i++){  /*count up the number of segments in the beam*/
    if(i<ratPoint->Nsegs[0]){
      if(!ratPoint->psf){
        ddthe=ratPoint->Ldiv/2.0-((double)i+0.5)*ratPoint->res;
      }else{              /*we have a gaussian wavefront*/
        ddthe=ratPoint->Ldiv/2.0*(double)(annuli-(i-1))/(double)ann;
      }
      ratPoint->Nsegs[i]=(int)(2.0*pie*sin(ddthe)/(double)ratPoint->res);
      ratPoint->Npixs+=ratPoint->Nsegs[i];
      ratPoint->dthe[i-1]=ddthe;
      ratPoint->dtha[i-1]=2.0*pie/(double)ratPoint->Nsegs[i];
    }else if(i==ratPoint->Nsegs[0]){
      ratPoint->Npixs++;
      ratPoint->Nsegs[i]=1;
      ratPoint->dthe[i-1]=0;
      ratPoint->dtha[i-1]=2.0*pie;
    }
    Nannuli++;
  }
  if(Nannuli!=annuli){
    printf("Some annuli counting error %d %d\n",Nannuli,annuli);
    exit(1);
  }
  if(ratPoint->psf){   /*then we need the divergence to the very edge*/
    ratPoint->Ediv=ratPoint->Ldiv;
    ratPoint->Ldiv=ratPoint->Ldiv*(double)annuli/(double)ann;
  }

  return(annuli);
} /*setPixelNumbers*/


/*#######################################################################################*/
/*Set the optical spread function*/

void setSpreadFunction(RatControl *ratPoint,int annuli)
{
  int i=0,rayN=0;
  double sigma=0;
  double gaussian(double,double,double);

  /*find the number of rays in each annulus*/
  rayN=ratPoint->rayN[0];
  if(ratPoint->rayN){
    free(ratPoint->rayN);
    ratPoint->rayN=NULL;
  }
  if(!(ratPoint->rayN=(int *)calloc(annuli,sizeof(int)))){
    printf("error in ray annuli number allocation.\n");
    exit(1);
  }
  if(!ratPoint->psf){   /*then it is equal*/
    for(i=0;i<annuli;i++){
      ratPoint->rayN[i]=rayN;
    }
    ratPoint->meanrayN=(float)rayN;
  }else{  /*we have a gausian spread*/
    ratPoint->meanrayN=0;
    sigma=ratPoint->Ediv/4.0;
    for(i=0;i<annuli;i++){   /*annulus 0 is the outside edge, annuli-1 the centre*/
      ratPoint->rayN[annuli-i-1]=(int)(2.0*(double)rayN*gaussian((double)i*(double)ratPoint->res,sigma,0)+0.5);
      /*ratPoint->rayN[annuli-i-1]=(int)(2.0*(double)rayN*exp(-1.0*(double)(i*i)*ratPoint->res*ratPoint->res/(2.0*sigma*sigma))+0.5);*/
      ratPoint->meanrayN+=(float)(ratPoint->rayN[i]*ratPoint->Nsegs[i+1]);
    }
    ratPoint->meanrayN/=(float)ratPoint->Npixs;
  }
  return;
}/*setSpreadFunction*/


/*#######################################################################################*/
/*set the pulse length*/

void setPulseLength(RatControl *ratPoint,int pBand)
{
  unsigned long int i=0,halfPoint=0;
  long int place=0;
  float E=0;
  double sigma=0,energy=0;
  double gaussian(double,double,double);
  double logNormal(double,double,double);

  if(!ratPoint->pulse){
    TIDY(ratPoint->EpLength);
    ratPoint->EpLength=ialloc(ratPoint->pBands,"pulse bin length",0);
    ratPoint->pulse=fFalloc(ratPoint->pBands,"laser pulse",0);
  }
  
  if(ratPoint->Pres==-1.0)ratPoint->Pres=ratPoint->bin_L/6.0;  /*over Nyquist sample pulse REMEMBER resolution is twice bin length*/
  ratPoint->EpLength[pBand]=0;

  if(ratPoint->funk==gaussian){
    printf("Gaussian pulse\n");
    sigma=(double)ratPoint->Plength[pBand]/4.0;
    /*calculate pulse length*/
    for(i=0;;i++){
      E=(float)ratPoint->funk((double)i*(double)ratPoint->Pres,sigma,0);
      /*E=exp(-1.0*(float)(i*i)*ratPoint->Pres*ratPoint->Pres/(2.0*sigma*sigma))/(sigma*sqrt(2.0*M_PI));*/
      if(E<=THRESHOLD){
        ratPoint->EpLength[pBand]=2*i;
        halfPoint=i;
        break;
      }
    }
  }else if(ratPoint->funk==logNormal){
    printf("Log normal pulse\n");
    sigma=(double)ratPoint->Plength[pBand];
    /*calculate pulse length*/
    for(i=(int)(sigma)+1;;i++){
      E=(float)ratPoint->funk((double)i*(double)ratPoint->Pres,sigma,0);
      if(E<=THRESHOLD){
        ratPoint->EpLength[pBand]=i;
        break;
      }
    }
  }
  if(ratPoint->EpLength[pBand]<=0){
    printf("Oh dear pulse array is %d long\n",ratPoint->EpLength[pBand]);
    exit(1);
  }
  if(!(ratPoint->pulse[pBand]=(float *)calloc(ratPoint->EpLength[pBand],sizeof(float)))){
    printf("error in pulse shape allocation.\n");
    exit(1);
  }
  /*set pulse shape*/
  if(ratPoint->funk==gaussian){
    for(i=0;i<ratPoint->EpLength[pBand];i++){
      place=(long int)((long int)i-(long int)halfPoint);
      ratPoint->pulse[pBand][i]=(float)ratPoint->funk((double)place*(double)ratPoint->Pres,sigma,0)*ratPoint->Pres;
      /*fprintf(opoo,"%f %f\n",(double)place*(double)ratPoint->Pres,ratPoint->pulse[pBand][i]);*/

    }
  }else if(ratPoint->funk==logNormal){
    for(i=0;i<ratPoint->EpLength[pBand];i++){
      place=(long int)i;
      ratPoint->pulse[pBand][i]=(float)ratPoint->funk((double)place*(double)ratPoint->Pres,sigma,0.0)*ratPoint->Pres;
    }

  }

  /*now normalise the pulse to have a sum of one at this sampling rate*/
  for(i=0;i<ratPoint->EpLength[pBand];i++)energy+=ratPoint->pulse[pBand][i];
  if(energy!=1.0){
    printf("The pulse is being rescaled by %f\n",1.0/energy);
    for(i=0;i<ratPoint->EpLength[pBand];i++)ratPoint->pulse[pBand][i]/=energy;
  }
  return;
}/*setPulseLength*/


/*#######################################################################################*/
/*Write the results to a binary file*/

void binaryResults(RatResults *ratRes,RatControl *ratPoint,int n,float theta,float thata,double *refl,float *material,int sky)
{
  double *header2=NULL,*jimlad=NULL,*jamlad=NULL;
  float *matlad=NULL,*mitlad=NULL;
  int *header1=NULL;
  int i=0,j=0;
  int *Header1=NULL;             /*These are all for holding the byte swapped data*/
  double *Header2=NULL;
  int zeroN=0,jc=0;  /*number of zeroes encountered, and comp pos*/
  char zeroI=0;    /*1 if we hace encounetered a zero, otherwise 0*/
  int *compheader=NULL;    /*compression map array*/

  /*Write the headers final time through and the results as we come to them*/
  /*A process that has not finished will leave a file with a blank header*/
  if(n>=0){
    /* THE DATA */
    /*write results if it is not the first time through*/

    /*reflectance information*/
    matlad=falloc((ratPoint->bins*ratPoint->nMat*2+1)*(ratPoint->withinSegs+ratPoint->withoutSegs),"material",0);
    if(ratPoint->encod){ 
      jamlad=dalloc(ratPoint->bins*ratPoint->nBands*(ratPoint->n+1)*(ratPoint->withinSegs+ratPoint->withoutSegs),"jamlad",0);
      jc=0;
      zeroN=0;
      zeroI=0;
//fprintf(stdout,"# %f %f\n",theta*180.0/M_PI,thata*180.0/M_PI);
      for(j=0;j<ratPoint->bins*ratPoint->nBands*(ratPoint->n+1)*(ratPoint->withinSegs+ratPoint->withoutSegs);j++){
//fprintf(stdout,"%d %f\n",j,refl[j]);
        if(refl[j]==0){
          zeroN++;  /*output -1*zeroes instead of a line of zeroes*/
          if(!zeroI)zeroI=1;
        }else if(refl[j]>0){
          if(zeroI){
            jamlad[jc]=(double)(-1*zeroN);
            zeroI=0;  /*reset zero counter*/
            zeroN=0;
            jc++;
          }
          jamlad[jc]=refl[j]/(double)((float)ratPoint->Npixs*ratPoint->meanrayN);  /*normalise the reflectances*/
          jc++;
        }
      }
      if(zeroI){
        jamlad[jc]=(double)(-1*zeroN);
        jc++;
      }
      ratRes[n].compLeng[0]=jc+2;

      if(!(jimlad=(double *)calloc(ratRes[n].compLeng[0],sizeof(double)))){
        printf("error in binary results allocation.\n");
        exit(1);
      }
      jimlad[0]=theta;
      jimlad[1]=thata;
      for(j=2;j<ratRes[n].compLeng[0];j++){
        jimlad[j]=jamlad[j-2];
      }

      jc=0;
      zeroI=0;
      zeroN=0;
      for(j=0;j<ratPoint->bins*ratPoint->nMat*2*(ratPoint->withinSegs+ratPoint->withoutSegs);j++){
        if(material[j]==0){
          zeroN++;  /*output -1*zeroes instead of a line of zeroes*/
          if(!zeroI)zeroI=1;
        }else if(material[j]>0){
          if(zeroI){
            matlad[jc]=(float)(-1*zeroN);
            zeroI=0; /*reset zero counter*/
            zeroN=0;
            jc++;
          }
          matlad[jc]=material[j]*100.0/((float)ratPoint->Npixs*ratPoint->meanrayN*(float)ratPoint->nBands); /*normalise*/
          jc++;
        }
      }
      /*we must do an extra step for the sky*/
      if(sky==0){
        zeroN++;
        if(!zeroI)zeroI=1;
      }else if(sky>0){
        if(zeroI){
          matlad[jc]=(float)(-1*zeroN);
          jc++;
        }
        matlad[jc]=sky*100.0/((float)ratPoint->Npixs*ratPoint->meanrayN); /*normalise*/
        zeroN=0;
        zeroI=0;
        jc++;
      }
      if(zeroI){
        matlad[jc]=-1*zeroN;
        jc++;
      }
      ratRes[n].compLeng[1]=jc;
      if(!(mitlad=(float *)calloc(ratRes[n].compLeng[1],sizeof(float)))){
        printf("error in binary results allocation.\n");
        exit(1);
      }
      for(j=0;j<ratRes[n].compLeng[1];j++){
        mitlad[j]=matlad[j];
      }
      /*now work out the compression map*/
      if(n==0){
        ratRes[n].coords[0]=(int)(ratPoint->headerLength*sizeof(int)+ratPoint->header2Length*sizeof(double)+\
          ratPoint->NZscans*ratPoint->NAscans*3*sizeof(int)+sizeof(double));
      }else if(n>0){
        ratRes[n].coords[0]=(int)(ratRes[n-1].coords[2]+(long int)sizeof(double));
      }
      ratRes[n].coords[1]=(int)(ratRes[n].coords[0]+ratRes[n].compLeng[0]*sizeof(double));
      ratRes[n].coords[2]=(int)(ratRes[n].coords[1]+ratRes[n].compLeng[1]*sizeof(float));
      //fprintf(stdout,"before %d Map %d %d %d\n",n,ratRes[n].coords[0],ratRes[n].coords[1],ratRes[n].coords[2]);
      //fprintf(stdout,"lengths %d %d %d\n",n,ratRes[n].compLeng[0],ratRes[n].compLeng[1]);
    }

    if(ratPoint->byteord){   /*if the computer is little endian swap bytes before writting*/
      jimlad=doSwap(jimlad,ratRes[n].compLeng[0]);
      matlad=floSwap(matlad,ratRes[n].compLeng[1]);
    }

    if(fseek(ratPoint->Soutput,(long)ratRes[n].coords[0],SEEK_SET)){printf("fseek error for jimlad %d\n",n);exit(1);}
    if(fwrite(&(jimlad[0]),sizeof(double),ratRes[n].compLeng[0],ratPoint->Soutput)!=ratRes[n].compLeng[0]){
      printf("Lidar data not written %d\n",n);
      exit(1);
    }
    /*material information*/
    if(fseek(ratPoint->Soutput,(long)ratRes[n].coords[1],SEEK_SET)){printf("fseek error for matlad %d\n",n);exit(1);}
    if(fwrite(&(matlad[0]),sizeof(float),ratRes[n].compLeng[1],ratPoint->Soutput)!=ratRes[n].compLeng[1]){
      printf("Material data not written %d\n",n);
      exit(1);
    }

    TIDY(jimlad);
    TIDY(matlad);
    TIDY(jamlad);
    TIDY(mitlad);
    /* THE DATA */
  }else if(n<0){   /*then deal with the header*/
    /* THE HEADER */
    header1=ialloc(ratPoint->headerLength,"int header",0);
    header2=dalloc(ratPoint->header2Length,"double header",0);
    /*arrange the int header*/
    if(!ratPoint->encod){
     fprintf(stderr,"Must run length encode\n");
     exit(1);
    }else if(ratPoint->encod) header1[0]=18;   /*SALCA format*/
    header1[1]=ratPoint->headerLength;
    header1[2]=ratPoint->header2Length;
    header1[3]=ratPoint->bins;
    header1[4]=ratPoint->NZscans;
    header1[5]=ratPoint->NAscans;
    header1[6]=ratPoint->n;
    header1[7]=ratPoint->nBands;
    header1[8]=ratPoint->nMat;
    header1[9]=ratPoint->withinSegs;
    header1[10]=ratPoint->withoutSegs;
    header1[11]=ratPoint->pBands;
    for(i=0;i<ratPoint->pBands;i++){
      if((12+i)>=ratPoint->headerLength){
        fprintf(stderr,"Balls\n");
        exit(1);
      }
      header1[12+i]=ratPoint->EpLength[i];
    }

    /*decimal header*/
    header2[0]=ratPoint->min_R;
    header2[1]=ratPoint->max_R;
    header2[2]=ratPoint->bin_L;
    for(i=0;i<3;i++){
      header2[i+3]=ratPoint->from[i];
    }
    if(!ratPoint->psf){
      header2[6]=ratPoint->Ldiv;
    }else{
      header2[6]=ratPoint->Ediv;
    }
    header2[7]=ratPoint->div*M_PI/180;  /*Convert div to radians for analysis*/
    header2[8]=ratPoint->divSt;
    header2[9]=ratPoint->iZen;
    header2[10]=ratPoint->fZen;
    header2[11]=ratPoint->iAz;
    header2[12]=ratPoint->fAz;
    header2[13]=ratPoint->squint;
    for(i=14;i<14+ratPoint->nBands;i++){
      header2[i]=ratPoint->wavelength[i-14];
    }
    for(i=0;i<ratPoint->pBands;i++)header2[14+ratPoint->nBands+i]=ratPoint->Plength[i];
    header2[14+ratPoint->nBands+ratPoint->pBands]=ratPoint->Pres;
    if((14+ratPoint->nBands+ratPoint->pBands)>=ratPoint->header2Length){
      fprintf(stderr,"Double balls\n");
      exit(1);
    }

    /*if the computer is big endian swap bytes before writting*/
    if(!ratPoint->byteord){
      Header1=ialloc(ratPoint->headerLength,"integer header",0);
      Header2=dalloc(ratPoint->header2Length,"double header",0);
      for(i=0;i<ratPoint->headerLength;i++)Header1[i]=header1[i];
      for(i=0;i<ratPoint->header2Length;i++)Header2[i]=header2[i];
    }else{
      Header1=intSwap(header1,ratPoint->headerLength);
      Header2=doSwap(header2,ratPoint->header2Length);
    }
    if(fseek(ratPoint->Soutput,0,SEEK_SET)){printf("fseek error for int header\n");exit(1);}
    if(fwrite(&(Header1[0]),sizeof(int),ratPoint->headerLength,ratPoint->Soutput)!=ratPoint->headerLength){
      printf("Data int header not written\n");
      exit(1);
    }
    /*if(fseek(ratPoint->Soutput,(long int)(ratPoint->headerLength*sizeof(int)),SEEK_SET)){printf("fseek error for double header\n");exit(1);}*/
    if(fwrite(&(Header2[0]),sizeof(double),ratPoint->header2Length,ratPoint->Soutput)!=ratPoint->header2Length){
      printf("Data double header not written\n");
      exit(1);
    }
    TIDY(header1);
    TIDY(header2);
    TIDY(Header1);
    TIDY(Header2);
    /* THE HEADER */

    /* MAPS */
    /*now, write out the compression map*/
    /*get the map into one array for ease of writting and fseeking*/
    if(!(compheader=(int *)calloc(3*ratPoint->NZscans*ratPoint->NAscans,sizeof(int)))){
      printf("error allocating compression map array\n");
      exit(1);
    }
    /*set initial positions*/
    for(i=0;i<ratPoint->NAscans*ratPoint->NZscans;i++){
      compheader[i*3]=ratRes[i].coords[0];
      compheader[i*3+1]=ratRes[i].coords[1];
      compheader[i*3+2]=ratRes[i].coords[2];
    }
    if(ratPoint->byteord)compheader=intSwap(compheader,3*ratPoint->NAscans*ratPoint->NZscans);
    /*if(fseek(ratPoint->Soutput,(long int)(ratPoint->headerLength*sizeof(int)+ratPoint->header2Length*sizeof(double)),SEEK_SET)){
      printf("fseek error for compression header\n");exit(1);}*/
    if(fwrite(&(compheader[0]),sizeof(int),3*ratPoint->NAscans*ratPoint->NZscans,ratPoint->Soutput)!=\
      3*ratPoint->NAscans*ratPoint->NZscans){
      printf("error writting compression map\n");
      exit(1);
    }
    TIDY(compheader);
    for(i=0;i<ratPoint->pBands;i++){
      if(ratPoint->Plength[i]>0){  /*write the pulse to the file's end*/
        if(ratPoint->byteord)ratPoint->pulse[i]=floSwap(ratPoint->pulse[i],ratPoint->EpLength[i]);
        if(fseek(ratPoint->Soutput,(long)ratRes[ratPoint->NAscans*ratPoint->NZscans-1].coords[2]+sizeof(float),SEEK_SET)){
          printf("fseek error for laser pulse\n");exit(1);}
        if(fwrite(&(ratPoint->pulse[i][0]),sizeof(float),ratPoint->EpLength[i],ratPoint->Soutput)!=ratPoint->EpLength[i]){
          printf("error writting laser pulse shape\n");
          exit(1);
        }
      }
    }/*pulse writting*/
  }/*header or data test*/
 /* if(byteHold)free(byteHold); */
}/*binaryResults*/


/*####################################################################################*/
/*to see if there is anything within a given distance*/

void inaTree(RatControl *ratPoint,RatResults *ratRes,int i)
{
  int j=0,ind=0;

  /*we are only checking the first bin, so no need to step through range bins*/
  for(j=0;j<ratPoint->nBands;j++){
    if(ratRes[i].refl[j*(ratPoint->n+1)]>0){
      printf("\nThere is an object within %f units\n",(ratPoint->min_R+ratPoint->bin_L)/2.0);
      ind=1;
      exit(2);
    }
  }
  if(!ind&&i==ratPoint->NZscans*ratPoint->NAscans-1){
    printf("\nThere does not seem to be anything within %f units\n",(ratPoint->min_R+ratPoint->bin_L)/2.0);
    printf("You're probably not within a tree, please continue\n");
  }
}/*inaTree*/


/*####################################################################################*/
/*To write the data to an ascii output*/

void asciiWrite(RatResults *ratRes,RatControl *ratPoint,int n)
{
  int i=0,j=0,k=0,m=0;
  int reflLength=0,materialLength=0;

  reflLength=ratPoint->bins*ratPoint->nBands*(ratPoint->n+1);
  materialLength=ratPoint->bins*ratPoint->nMat*2;

  /*scale the reflectances*/
  /* and for now display the results on the screen*/
  /* but also put it into a file*/
  fprintf(ratPoint->output,"\n%f %f\n\n",ratRes[n].theta*180/M_PI,ratRes[n].thata*180/M_PI);
  for(i=0;i<ratPoint->bins;i++){
    fprintf(ratPoint->output,"%d ",(int)((double)i*ratPoint->bin_L+ratPoint->min_R));
    for(j=0;j<ratPoint->nBands;j++){
      for(m=0;m<ratPoint->withinSegs+ratPoint->withoutSegs;m++){
        fprintf(ratPoint->output," Seg%d ",m);
        fprintf(ratPoint->output,"%f ",ratRes[n].refl[m*reflLength+(i*ratPoint->nBands+j)*(ratPoint->n+1)]/\
          (double)((float)ratPoint->Npixs*ratPoint->meanrayN));
        for(k=0;k<ratPoint->n;k++){
          fprintf(ratPoint->output,"%f ",ratRes[n].refl[m*reflLength+(i*ratPoint->nBands+j)*(ratPoint->n+1)+k+1]/\
            (double)((float)ratPoint->Npixs*ratPoint->meanrayN));
        }
        fprintf(ratPoint->output,"  mat ");
        for(k=0;k<2*ratPoint->nMat;k++){
          fprintf(ratPoint->output,"%d ",(int)(ratRes[n].material[m*materialLength+i*ratPoint->nMat*2+k]*100.0/\
            (double)((float)ratPoint->Npixs*ratPoint->meanrayN)));
        }
      }
    }
    fprintf(ratPoint->output,"%d ",(int)(ratRes[n].sky*100.0/((float)ratPoint->Npixs*ratPoint->meanrayN)));
    /*a bit unneccesarily repetative, but where else will it go?*/
    fprintf(ratPoint->output,"\n");
  }
  fprintf(ratPoint->output,"\n");
}/*asciiWrite*/


/*################################################################*/
/* Lewis' help. user arguments for help */

void RATuserPrintOptions(RATobj *ratObj){
  fprintf(stderr,"[-Ufrom starting point]\n[-Ufov FOV]\n[-Ubeam beam_width]\n[-Ustep step_size]\n[-Ures pixel size]\n[-Urays mean rays per pixel]\n[-psf gaussian ray spread]\n[-split withinSegs withoutSegs]\n[-pulseLength width in units, default is gaussian]\n[-diffPulses l1 l2;   two pulse lengths for two bands]\n[-pres resolution in units, default 1/4 of range resolution]\n[-lognormal, use log normal pulse shape]\n[-Ulidar min_range max_range bin_length]\n[-Uoutput lidar_output_name]\n[-Uzoom image_zoom; image on/off]\n[-Uscan initial_zenith final_zenith initial_azimuth final_azimuth]\n[-UzoomH hemispherical_image_soom]\n[-UsetSun x y z]\n[-Uwhere space test]\n[-encode; rn length encoder]\n[-Uascii ascii output]\n[-Uwhere/-Utest tests for obstructions]\n-rint integer; random number seed\n-squint angle;   SALCA squint angle, degrees\n\n");
  return;
}


/*################################################################*/
/*read the command line*/

int RATuserParse(RATobj *ratObj,int thisarg,int argc,char **argv,void *info)
{
  int j=0;
  int numberOfArguments=-1,i;
  double atof();
  RatControl *ratPoint;
  double pie=M_PI;
  void checkArguments(int,int,int,char *);

  ratPoint=(RatControl *)info;

  if(!(strcmp(argv[thisarg],"-Ufrom"))){
    numberOfArguments=3;
    checkArguments(numberOfArguments,thisarg,argc,"-Ufrom");
    for(i=0;i<numberOfArguments;i++){
      ratPoint->from[i] = atof(argv[thisarg+1+i]);
    }
  }
  else if (!(strcmp(argv[thisarg],"-Ufov"))){
    numberOfArguments=1;
    checkArguments(numberOfArguments,thisarg,argc,"-Ufov");
    ratPoint->div=atof(argv[thisarg+1]);
  }
  else if (!(strcmp(argv[thisarg],"-Ubeam"))){
    numberOfArguments=1;
    checkArguments(numberOfArguments,thisarg,argc,"-Ubeam");
    ratPoint->Ldiv=atof(argv[thisarg+1])*M_PI/180;
  }
  else if (!(strcmp(argv[thisarg],"-Ustep"))){
    numberOfArguments=1;
    checkArguments(numberOfArguments,thisarg,argc,"-Ustep");
    ratPoint->divSt = pie*atof(argv[thisarg+1])/180;
  }
  else if (!(strcmp(argv[thisarg],"-Ures"))){
    numberOfArguments=1;
    checkArguments(numberOfArguments,thisarg,argc,"-Ures");
    ratPoint->res = pie*atof(argv[thisarg+1])/180;
  }
  else if (!(strcmp(argv[thisarg],"-Urays"))){
    numberOfArguments=1;
    checkArguments(numberOfArguments,thisarg,argc,"-Urays");
    ratPoint->rayN[0]=atoi(argv[thisarg+1]);
  }
  else if (!(strcmp(argv[thisarg],"-psf"))){
    numberOfArguments=0;
    checkArguments(numberOfArguments,thisarg,argc,"-psf");
    ratPoint->psf=1;
  }
  else if (!(strcmp(argv[thisarg],"-Ulidar"))){
    numberOfArguments=3;
    checkArguments(numberOfArguments,thisarg,argc,"-Ulidar");
    ratPoint->min_R=atof(argv[thisarg+1]);
    ratPoint->max_R=atof(argv[thisarg+2]);
    ratPoint->bin_L=atof(argv[thisarg+3]);
  }
  else if (!(strcmp(argv[thisarg],"-Uoutput"))){
    numberOfArguments=1;
    checkArguments(numberOfArguments,thisarg,argc,"-Uoutput");
    strcpy(ratPoint->Loutput,argv[thisarg+1]);
  }
  else if (!(strcmp(argv[thisarg],"-Uzoom"))){
    numberOfArguments=1;
    checkArguments(numberOfArguments,thisarg,argc,"-Uzoom");
    ratPoint->zoom = atof(argv[thisarg+1]);
  }
  else if(!(strcmp(argv[thisarg],"-Uscan"))){
    numberOfArguments=4;
    checkArguments(numberOfArguments,thisarg,argc,"-Uscan");
    ratPoint->iZen=pie*atof(argv[thisarg+1])/180.0;
    ratPoint->fZen=pie*atof(argv[thisarg+2])/180.0;
    ratPoint->iAz=pie*atof(argv[thisarg+3])/180.0;
    ratPoint->fAz=pie*atof(argv[thisarg+4])/180.0;
  }
  else if (!(strcmp(argv[thisarg],"-Uascii"))){
    numberOfArguments=0;
    checkArguments(numberOfArguments,thisarg,argc,"-Uascii");
    ratPoint->Ascii = 1;
  }
  else if (!(strcmp(argv[thisarg],"-encode"))){
    numberOfArguments=0;
    checkArguments(numberOfArguments,thisarg,argc,"-encode");
    ratPoint->encod = 1;
  }
  else if (!(strcmp(argv[thisarg],"-Uwhere"))||!(strcmp(argv[thisarg],"-Utest"))){
    numberOfArguments=0;
    checkArguments(numberOfArguments,thisarg,argc,"-where");
    ratPoint->test = 1;
  }
  else if (!(strcmp(argv[thisarg],"-split"))){
    numberOfArguments=2;
    for(i=0;i<numberOfArguments;i++){
      if(thisarg+1+i>=argc){
        fprintf(stderr,"error in number of arguments for -Uwhere option: none required\n");
        exit(1);
      }
    }
    ratPoint->withinSegs=atoi(argv[thisarg+1]);
    ratPoint->withoutSegs=atoi(argv[thisarg+2]);
  }else if(!(strcmp(argv[thisarg],"-pulseLength"))){
    numberOfArguments=1;
    checkArguments(numberOfArguments,thisarg,argc,"-pulseLength");
    ratPoint->pBands=1;
    TIDY(ratPoint->Plength);
    ratPoint->Plength=falloc(ratPoint->pBands,"pulse lengths",0);
    ratPoint->Plength[0]=atof(argv[thisarg+1]);
  }else if(!(strcmp(argv[thisarg],"-pres"))){
    numberOfArguments=1;
    checkArguments(numberOfArguments,thisarg,argc,"-pres");
    ratPoint->Pres=atof(argv[thisarg+1]);
  }
  else if(!(strcmp(argv[thisarg],"-lognormal"))){
    numberOfArguments=0;
    checkArguments(numberOfArguments,thisarg,argc,"-lognormal");
    ratPoint->funk=logNormal;
  }
  else if(!(strcmp(argv[thisarg],"-rint"))){
    numberOfArguments=1;
    checkArguments(numberOfArguments,thisarg,argc,"-rint");
    ratPoint->rint=atoi(argv[thisarg+1]);
  }else if(!(strcmp(argv[thisarg],"-diffPulses"))){    /*SALCA*/
    numberOfArguments=2;
    /*checkArguments(1,thisarg,argc,"-diffPulses");*/
    ratPoint->pBands=2;   /*atoi(argv[++i]);*/
    checkArguments(ratPoint->pBands,thisarg,argc,"-diffPulses");
    TIDY(ratPoint->Plength);
    ratPoint->Plength=falloc(ratPoint->pBands,"pulse lengths",0);
    for(j=0;j<ratPoint->pBands;j++)ratPoint->Plength[j]=atof(argv[thisarg+j+1]);
  }else if(!(strcmp(argv[thisarg],"-squint"))){
    numberOfArguments=1;
    checkArguments(numberOfArguments,thisarg,argc,"-squint");
    ratPoint->squint=atof(argv[thisarg+1])*M_PI/180.0;  /*convert to rads*/
  }
  return(numberOfArguments);
}/*RATuserParse*/


/*
########################
*
* example of a signal interrupt
*
*/

void userSignals(){
  /* always call RATsignalInterrupt */
  signal(SIGUSR1,RATsignalInterrupt);
  signal(SIGUSR2,RATsignalInterrupt);
  signal(SIGSTOP,RATsignalInterrupt);
}

void RATuserInterrupt(RATobj *ratObj,int sig){
  switch(sig){
  case SIGUSR1:
    fprintf(stderr,"rat:\tsignal interrupt %d for process %d\n",sig,RATgetPID());
    break;
  case SIGUSR2:
    fprintf(stderr,"rat:\tsignal interrupt %d for process %d\n",sig,RATgetPID());
    break;
  case SIGSTOP:
    fprintf(stderr,"rat:\tsignal interrupt %d for process %d\n",sig,RATgetPID());
    exit(1);
    break;
  }
  return;
}


/*##############################################*/
/*interaction type*/

char *interactionType(int i){
  static char r[] = "reflectance",t[]="transmittance",n[]="nothing";
  char *out;
  switch(i){
  case 0:
    out=&r[0];
    break;
  case 1:
    out=&t[0];
    break;
  default:
    out=&n[0];
    break;  
  }
  return(out);
}


/*##############################################*/
/*tidy up the arrays ready for closing*/

void tidyUpArrays(RatControl *ratPoint,RATobj *ratObj,RATtree *ratTree,RatResults *ratRes)
{
  int i=0;

  if(ratPoint->output){
    fclose(ratPoint->output);
    ratPoint->output=NULL;
  }
  if(ratPoint->Soutput){
    fclose(ratPoint->Soutput);
    ratPoint->Soutput=NULL;
  }

  TIDY(ratObj);
  TIDY(ratTree);
  if(ratRes){
    for(i=0;i<ratPoint->NAscans*ratPoint->NZscans;i++){
      TIDY(ratRes[i].refl);
      TIDY(ratRes[i].material);
    }
    free(ratRes);
    ratRes=NULL;
  }
  if(ratPoint){
    TIDY(ratPoint->wavelength);
    TIDY(ratPoint->Nsegs);
    TTIDY((void **)ratPoint->pulse,ratPoint->pBands);
    TIDY(ratPoint->EpLength);
    TIDY(ratPoint->dthe);
    TIDY(ratPoint->dtha);
    free(ratPoint);
    ratPoint=NULL;
  }
  return;
} /*tidyUpArrays*/


/*#####################################################*/
/*clean up after drawing an image*/

void cleanPics(RatControl *ratPoint)
{

  if(ratPoint->InSegs){
    free(ratPoint->InSegs);
    ratPoint->InSegs=NULL;
  }
  if(ratPoint->idthe){
    free(ratPoint->idthe);
    ratPoint->idthe=NULL;
  }
  if(ratPoint->idtha){
    free(ratPoint->idtha);
    ratPoint->idtha=NULL;
  }

  return;
}/*cleanPics*/


/*#####################################################*/
/*open outpout file ready*/

void openOutput(RatControl *ratPoint)
{
  char namen[200];

  if((ratPoint->Soutput=fopen(strcat(strcpy(namen,ratPoint->Loutput),".data"),"wb"))==NULL){
    printf("Error opening data output\n");
    exit(1);
  }
  if(ratPoint->Ascii){
    if((ratPoint->output=fopen(strcat(strcpy(namen,ratPoint->Loutput),".txt"),"w"))==NULL){
      printf("Error opening output\n");
      exit(1);
    }
  }
  return;
}/*openOutput*/


/*THE END*/
/*#######################################################*/

